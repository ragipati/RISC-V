// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, Nawras Altaleb, Sandeep Ragipati
// -------------------------------------------------

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
#ifndef RISCV_ISS_Interrupts_H_
#define RISCV_ISS_Interrupts_H_

#include "systemc.h"
#include "../Test_Environment/Environment/Interfaces/Interfaces.h"
#include "../Test_Environment/Environment/RISCV_commons/Memory_Interfaces.h"
#include "../Test_Environment/Environment/RISCV_commons/Defines.h"
#include "../Test_Environment/Environment/RISCV_commons/Utilities.h"
#include "CPU_Interfaces.h"
#include <array>

// Adjusts code to be appropriate for the LUBIS tool
// Working ESL-Description -> //#define VIP_GEN
// Properties can be generated -> #define VIP_GEN
#define VIP_GEN

#define PRIVCODE_ECALL      0x000
#define PRIVCODE_EBREAK     0x001
#define PRIVCODE_URET       0x002
#define PRIVCODE_SRET       0x102
#define PRIVCODE_MRET       0x302
#define PRIVCODE_WFI        0x105
#define PRIVCODE_SFENCEVMA  0x009


#define MCSR_MSTATUS    0x300
#define MCSR_MISA       0x301
#define MCSR_MIE        0x304
#define MCSR_MTVEC      0x305
#define MCSR_MSCRATCH   0x340
#define MCSR_MEPC       0x341
#define MCSR_MCAUSE     0x342
#define MCSR_MTVAL      0x343
#define MCSR_MIP        0x344
#define MCSR_MVENDORID  0xF11
#define MCSR_MARCHID    0xF12
#define MCSR_MIMPID     0xF13
#define MCSR_MHARTID    0xF14

#define MSTATUS_MIE(x)  ((x) & 0x00000008)
#define MTRAP_ML12I(x)  ((x) & 0x00001000)
#define MTRAP_MEI(x)    ((x) & 0x00000800)
#define MTRAP_MSI(x)    ((x) & 0x00000008)
#define MTRAP_MTI(x)    ((x) & 0x00000080)

#define SHAMT5 0x02000000
#define JUMP_MASK 0xFFFFFFFE


class ISS_Interrupts : public sc_module {
public:
    //Constructor
    SC_HAS_PROCESS(ISS_Interrupts);

    ISS_Interrupts(sc_module_name name) :
            fromMemoryPort("fromMemoryPort"),
            toMemoryPort("toMemoryPort"),
            toRegsPort("toRegsPort"),
            fromRegsPort("fromRegsPort"),
            isa_ecall_Port("isa_ecall_Port"),
            ecall_isa_Port("ecall_isa_Port"),
            mip_isa_Port("mip_isa_Port"),
            sysRES(true),
            excFlag(false),
            pcReg(0) {
        SC_THREAD(run);
    }

    // ports for communication with memory/decoder
    blocking_out<CUtoME_IF> toMemoryPort;
    blocking_in<MEtoCU_IF> fromMemoryPort;

    // ports for communication with register file
    master_in<std::array<unsigned int, 32>> fromRegsPort;
    master_out<RegfileWriteType> toRegsPort;

    // ports for communication with ecallHandler
    blocking_out<bool> isa_ecall_Port;
    blocking_in<bool> ecall_isa_Port;

    // port for communication with mip
    master_in<unsigned int> mip_isa_Port;

    // data for communication with memory
    CUtoME_IF toMemoryData;
    MEtoCU_IF fromMemoryData;

    // data for communication with register file
    RegfileWriteType regfileWrite;
    std::array<unsigned int, 32> regfile;

    // data for communication with ecallHandler
    bool sysRES;

    // exception flag
    bool excFlag = false;

    // ISS_Interrupts Phases
    enum Phases {
        execute, // decode the fetched instruction and do all the manipulations till writing back to the register file
        fetch    // fetch next instruction from memory
    };
    Phases phase, nextphase;

    // Other control signals:
    unsigned int encodedInstr;

    unsigned int aluOp1;
    unsigned int aluOp2;
    unsigned int aluResult;

    unsigned int pcReg;
    unsigned int pcTemp;

    CSRfileType csrfile;

    void run(); // thread

    //Decoding functions
    EncType getEncType(unsigned int encodedInstr) const;

    InstrType_Complete getInstrType(unsigned int encodedInstr) const;

    unsigned int getImmediate(unsigned int encodedInstr) const;

    ME_MaskType getMemoryMask(InstrType_Complete instr) const;

    ALUfuncType getALUfunc(InstrType_Complete instr) const;

    unsigned int getALUresult(ALUfuncType aluFunction, unsigned int operand1, unsigned int operand2) const;

    unsigned int getCSR(CSRfileType csrfile, unsigned int encodedInstr) const;

    unsigned int getBranchresult(unsigned int encodedInstr, unsigned int aluResult, unsigned int pcReg) const;

    unsigned int updateMCAUSE(CSRfileType csrfile, bool excFlag) const;

    unsigned int updateMSTATUS(CSRfileType csrfile, bool excFlag) const;

    unsigned int updateMEPC(CSRfileType csrfile, unsigned int pcReg, bool excFlag) const;

    unsigned int updatePC(CSRfileType csrfile, unsigned int pcReg, bool excFlag) const;

    unsigned int getALUresult_U(unsigned int encodedInstr, unsigned int pcReg, unsigned int imm) const;

};


void ISS_Interrupts::run() {
    nextphase = Phases::fetch;
    while (true) {
        phase = nextphase;
        // fetch next instruction
        if (phase == Phases::fetch) {
            // Set up memory access
            toMemoryData = (CUtoME_IF){.req = ME_RD, .mask = MT_W, .addrIn = pcReg, .dataIn = 0};

            toMemoryPort->write(toMemoryData, "FETCH_REQ"); //Send request to memory

            fromMemoryPort->read(fromMemoryData, "FETCH_DONE"); //Read encoded instruction from memory

            encodedInstr = fromMemoryData.loadedData;

            nextphase = Phases::execute;
        }

        if (phase == Phases::execute) {

            mip_isa_Port->master_read(csrfile.mip, "Read_MIP"); //Read mip status

            // Decode instruction
            if (getEncType(encodedInstr) == ENC_R) {
                /////////////////////////////////////////////////////////////////////////////
                //|  ID (RF_READ)   |        EX       |    ---------    |  WB (RF_WRITE)  |//
                /////////////////////////////////////////////////////////////////////////////

                fromRegsPort->master_read(regfile, "Read_ENC_R"); //Read register contents

                //Set-up operands for alu by reading from regfile
                aluOp1 = regfile[RS1_FIELD(encodedInstr)];
                aluOp2 = regfile[RS2_FIELD(encodedInstr)];

                aluResult = getALUresult(getALUfunc(getInstrType(encodedInstr)), aluOp1, aluOp2); ; //Compute result

                //Set up write back
                regfileWrite = (RegfileWriteType){.dst = RD_FIELD(encodedInstr), .dstData = aluResult};

                toRegsPort->master_write(regfileWrite, "Write_ENC_R"); //Perform write back

                //Set-up PC
                pcReg = pcReg + 4;

            } else if (getEncType(encodedInstr) == ENC_B) {
                /////////////////////////////////////////////////////////////////////////////
                //|  ID (RF_READ)   |        EX       |    ---------    |    ---------    |//
                /////////////////////////////////////////////////////////////////////////////

                fromRegsPort->master_read(regfile, "Read_ENC_B"); //Read register contents

                //Set-up operands for alu by reading from regfile
                aluOp1 = regfile[RS1_FIELD(encodedInstr)];
                aluOp2 = regfile[RS2_FIELD(encodedInstr)];

                aluResult = getALUresult(getALUfunc(getInstrType(encodedInstr)), aluOp1, aluOp2); //Compute result
                //Set-up PC
                pcTemp = getBranchresult(encodedInstr, aluResult, pcReg);

                if (pcTemp % 4 != 0){
                    csrfile.mcause = 0;
                    csrfile.mtval = pcTemp;
                    csrfile.mepc = pcReg;
                    excFlag = true;
                }
                else {
                    pcReg = pcTemp;
                }

            } else if (getEncType(encodedInstr) == ENC_S) {
                /////////////////////////////////////////////////////////////////////////////
                //|  ID (RF_READ)   |        EX       |       MEM       |    ---------    |//
                /////////////////////////////////////////////////////////////////////////////

                fromRegsPort->master_read(regfile, "Read_ENC_S"); //Read register contents

                //Set-up operands for alu by reading from regfile
                aluOp1 = regfile[RS1_FIELD(encodedInstr)];
                aluOp2 = getImmediate(encodedInstr);

                aluResult = getALUresult(getALUfunc(getInstrType(encodedInstr)), aluOp1, aluOp2); ; //Compute result

                if ((getInstrType(encodedInstr) == INSTR_SH && aluResult %2 != 0) ||
                    (getInstrType(encodedInstr) == INSTR_SW && aluResult % 4 != 0)) {
                    csrfile.mcause = 6;
                    csrfile.mtval = aluResult;
                    csrfile.mepc = pcReg;
                    excFlag = true;
                }
                else {
                    //prepare memory access
                    toMemoryData = (CUtoME_IF){.req = ME_WR, .mask = getMemoryMask(getInstrType(encodedInstr)), .addrIn = aluResult, .dataIn = regfile[RS2_FIELD(encodedInstr)]};

                    toMemoryPort->write(toMemoryData, "STORE"); // Store
                    //Set-up PC
                    pcReg = pcReg + 4;

                }

            } else if (getEncType(encodedInstr) == ENC_U) {
                /////////////////////////////////////////////////////////////////////////////
                //|        ID       |        EX       |    ---------    |  WB (RF_WRITE)  |//
                /////////////////////////////////////////////////////////////////////////////
                aluOp1 = pcReg;
                aluOp2 = getImmediate(encodedInstr);
                aluResult = getALUresult_U(encodedInstr, aluOp1, aluOp2);

                regfileWrite = (RegfileWriteType){.dst = RD_FIELD(encodedInstr), .dstData = aluResult};

                toRegsPort->master_write(regfileWrite, "Write_ENC_U"); //Perform write back

                //Set-up PC
                pcReg = pcReg + 4;

            } else if (getEncType(encodedInstr) == ENC_J) {
                /////////////////////////////////////////////////////////////////////////////
                //|        ID       |    ---------    |    ---------    |  WB (RF_WRITE)  |//
                /////////////////////////////////////////////////////////////////////////////

                //Set-up PC
                pcTemp = pcReg + getImmediate(encodedInstr);

                if (pcTemp % 4 != 0){
                    csrfile.mcause = 0;
                    csrfile.mtval = pcTemp;
                    csrfile.mepc = pcReg;
                    excFlag = true;
                }
                else {
                    //Set up write back
                    regfileWrite = (RegfileWriteType){.dst = RD_FIELD(encodedInstr), .dstData = pcReg + 4};
                    pcReg = pcTemp;
                    toRegsPort->master_write(regfileWrite, "Write_ENC_J"); //Perform write back
                }

            } else if (getEncType(encodedInstr) == ENC_I_I) {
                /////////////////////////////////////////////////////////////////////////////
                //|  ID (RF_READ)   |        EX       |    ---------    |  WB (RF_WRITE)  |//
                /////////////////////////////////////////////////////////////////////////////

                fromRegsPort->master_read(regfile, "Read_ENC_I_I"); //Read register contents

                //Set-up operands for alu by reading from regfile
                aluOp1 = regfile[RS1_FIELD(encodedInstr)];
                aluOp2 = getImmediate(encodedInstr);
                aluResult = getALUresult(getALUfunc(getInstrType(encodedInstr)), aluOp1, aluOp2); ; //Compute result

                if((getInstrType(encodedInstr) == INSTR_SLLI || getInstrType(encodedInstr) == INSTR_SRLI ||
                    getInstrType(encodedInstr) == INSTR_SRAI) && (encodedInstr & SHAMT5) != 0){
                    csrfile.mcause = 2;
                    csrfile.mtval = 0;
                    csrfile.mepc = pcReg;
                    excFlag = true;
                } else {
                    //Set up write back
                    regfileWrite = (RegfileWriteType) {.dst = RD_FIELD(encodedInstr), .dstData = aluResult};

                    toRegsPort->master_write(regfileWrite, "Write_ENC_I_I"); //Perform write back

                    //Set-up PC
                    pcReg = pcReg + 4;
                }
            } else if (getEncType(encodedInstr) == ENC_I_L) { // loads
                /////////////////////////////////////////////////////////////////////////////
                //|  ID (RF_READ)   |        EX       |       MEM       |  ID (RF_WRITE)  |//
                /////////////////////////////////////////////////////////////////////////////
                fromRegsPort->master_read(regfile, "Read_ENC_I_L"); //Read register contents

                //Set-up operands for alu by reading from regfile
                aluOp1 = regfile[RS1_FIELD(encodedInstr)];
                aluOp2 = getImmediate(encodedInstr);

                aluResult = getALUresult(getALUfunc(getInstrType(encodedInstr)), aluOp1, aluOp2); ; //Compute result

                if (((getInstrType(encodedInstr) == INSTR_LH || getInstrType(encodedInstr) == INSTR_LHU) && aluResult % 2 != 0)
                    || (getInstrType(encodedInstr) == INSTR_LW && aluResult % 4 != 0)){
                    csrfile.mcause = 4;
                    csrfile.mepc = pcReg;
                    csrfile.mtval = aluResult;
                    excFlag = true;
                }

                else {
                    //prepare memory access
                    toMemoryData = (CUtoME_IF){.req = ME_RD, .mask = getMemoryMask(getInstrType(encodedInstr)), .addrIn = aluResult, .dataIn = 0};

                    regfileWrite.dst = RD_FIELD(encodedInstr);
                    // Request load
                    toMemoryPort->write(toMemoryData, "LOAD_REQ");

                    // Load done
                    fromMemoryPort->read(fromMemoryData, "LOAD_DONE");

                    //Set up write back
                    regfileWrite.dstData = fromMemoryData.loadedData;

                    //Perform write back
                    toRegsPort->master_write(regfileWrite, "Write_ENC_I_L");

                    //Set-up PC
                    pcReg = pcReg + 4;


                }
            } else if (getEncType(encodedInstr) == ENC_I_J) {
                /////////////////////////////////////////////////////////////////////////////
                //|  ID (RF_READ)   |    ---------    |    ---------    |  WB (RF_WRITE)  |//
                /////////////////////////////////////////////////////////////////////////////

                fromRegsPort->master_read(regfile, "Read_ENC_I_J"); //Read register contents

                //Set-up PC
                pcTemp = (regfile[RS1_FIELD(encodedInstr)] + getImmediate(encodedInstr)) & 0xFFFFFFFE;

                if (pcTemp % 4 != 0){
                    csrfile.mcause = 0;
                    csrfile.mtval = pcTemp;
                    csrfile.mepc = pcReg;
                    excFlag = true;
                }
                else {
                    //Set up write back
                    regfileWrite = (RegfileWriteType){.dst = RD_FIELD(encodedInstr), .dstData = pcReg + 4};
                    pcReg = pcTemp;
                    //Perform write back
                    toRegsPort->master_write(regfileWrite, "Write_ENC_I_J");
                }

            } else if (getEncType(encodedInstr) == ENC_I_M) {
                /////////////////////////////////////////////////////////////////////////////
                //|  -------   |        -------       |       --------       |  --------  |//
                /////////////////////////////////////////////////////////////////////////////
                //Set-up PC
                pcReg = pcReg + 4;

            } else if (getEncType(encodedInstr) == ENC_I_S) {
                if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_CSRRW ||
                    getInstrType(encodedInstr) == InstrType_Complete::INSTR_CSRRS ||
                    getInstrType(encodedInstr) == InstrType_Complete::INSTR_CSRRC) {
                    /////////////////////////////////////////////////////////////////////////////
                    //|  ID (RF_READ)   |       ------     |     ------     |  WB (RF_WRITE)  |//
                    /////////////////////////////////////////////////////////////////////////////
                    fromRegsPort->master_read(regfile, "Read_ENC_I_S"); //Read register contents

                    //get operand for next csr by reading from regfile and read current csr value
                    aluOp1 = regfile[RS1_FIELD(encodedInstr)];
                    aluOp2 = getCSR(csrfile, encodedInstr);
                    aluResult = getALUresult(getALUfunc(getInstrType(encodedInstr)), aluOp1, aluOp2);

                    // write back to csr
                    if (getImmediate(encodedInstr) == MCSR_MSTATUS) {
                        if ((aluResult >> 11 & 3U) == 3) {              //only machine mode is supported
                            csrfile.mstatus = aluResult;
                        }
                        //csrfile.mstatus = aluResult;
                    } else if (getImmediate(encodedInstr) == MCSR_MIE) {
                        csrfile.mie = aluResult;
                    } else if (getImmediate(encodedInstr) == MCSR_MTVEC) {
                        csrfile.mtvec = aluResult & 0xFFFFFFFC;
                    } else if (getImmediate(encodedInstr) == MCSR_MEPC) {
                        csrfile.mepc = aluResult & 0xFFFFFFFC;
                    } else if (getImmediate(encodedInstr) == MCSR_MCAUSE) {
                        csrfile.mcause = aluResult;
                    } else if (getImmediate(encodedInstr)== MCSR_MSCRATCH){
                        csrfile.mscratch = aluResult;
                    } else if (getImmediate(encodedInstr) == MCSR_MISA){
                        csrfile.misa = 0x40000080;
                    } else if (getImmediate(encodedInstr) == MCSR_MTVAL){
                        csrfile.mtval = aluResult;
                    }

                    //Set up write back
                    regfileWrite = (RegfileWriteType){.dst = RD_FIELD(encodedInstr), .dstData = aluOp2};

                    toRegsPort->master_write(regfileWrite, "Write_ENC_I_S"); //Perform write back

                    //Set-up PC
                    pcReg = pcReg + 4;
                } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_CSRRWI ||
                           getInstrType(encodedInstr) == InstrType_Complete::INSTR_CSRRSI ||
                           getInstrType(encodedInstr) == InstrType_Complete::INSTR_CSRRCI) {
                    /////////////////////////////////////////////////////////////////////////////
                    //|    ------    |       ------      |       ------     |  WB (RF_WRITE)  |//
                    /////////////////////////////////////////////////////////////////////////////
                    //get operand for next csr by reading from regfile and read current csr value
                    aluOp1 = RS1_FIELD(encodedInstr);
                    aluOp2 = getCSR(csrfile, encodedInstr);
                    aluResult = getALUresult(getALUfunc(getInstrType(encodedInstr)), aluOp1, aluOp2);

                    // write back to csr
                    if (getImmediate(encodedInstr) == MCSR_MSTATUS) {
                        if ((aluResult >> 11 & 3U) == 3) {              //only machine mode is supported
                            csrfile.mstatus = aluResult;
                        }
                        //csrfile.mstatus = aluResult;
                    } else if (getImmediate(encodedInstr) == MCSR_MIE) {
                        csrfile.mie = aluResult;
                    } else if (getImmediate(encodedInstr) == MCSR_MTVEC) {
                        csrfile.mtvec = aluResult & 0xFFFFFFFC;
                    } else if (getImmediate(encodedInstr) == MCSR_MEPC) {
                        csrfile.mepc = aluResult & 0xFFFFFFFC;
                    } else if (getImmediate(encodedInstr) == MCSR_MCAUSE) {
                        csrfile.mcause = aluResult;
                    } else if (getImmediate(encodedInstr)== MCSR_MSCRATCH){
                        csrfile.mscratch = aluResult;
                    } else if (getImmediate(encodedInstr) == MCSR_MISA){
                        csrfile.misa = 0x40000080;
                    } else if (getImmediate(encodedInstr) == MCSR_MTVAL){
                        csrfile.mtval = aluResult;
                    }

                    //Set up write back
                    regfileWrite = (RegfileWriteType){.dst = RD_FIELD(encodedInstr), .dstData = aluOp2};

                    toRegsPort->master_write(regfileWrite, "Write_ENC_IS_S_CSRRI"); //Perform write back

                    //Set-up PC
                    pcReg = pcReg + 4;
                } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_ECALL) {
                    /////////////////////////////////////////////////////////////////////////////
                    //|    ------     |       ------      |       ------      |    ------     |//
                    /////////////////////////////////////////////////////////////////////////////
                        csrfile.mcause = 11;
                        csrfile.mepc = pcReg + 4;
                        excFlag = true;
                        isa_ecall_Port->write(true, "ECALL_REQ");
                        // wait for ecall to finish processing the ecall
                        ecall_isa_Port->read(sysRES, "ECALL_DONE");
#ifndef VIP_GEN
                        if(!sysRES) {
                            std::cout << "ECALL exit" << std::endl;
                            sc_stop();
                            wait(WAIT_TIME, SC_PS);
                        }
#endif
                       // pcReg = pcReg + 4;
                    } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_EBREAK) {
                        csrfile.mcause = 3;
                        csrfile.mepc = pcReg;
                        excFlag = true;
#ifndef VIP_GEN
                        std::cout << "EBREAK" << std::endl;
                        sc_stop();
                        wait(WAIT_TIME, SC_PS);
//                        wait(SC_ZERO_TIME);
#endif
                    } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_MRET) {
                        csrfile.mstatus = csrfile.mstatus | 0x00000008; // set MIE
                        pcReg = csrfile.mepc;
                    } else
                        pcReg = pcReg + 4;
                //Terminate if Unkown instr
                } else if (getEncType(encodedInstr) == ENC_ERR){
                    csrfile.mcause = 2;
                    csrfile.mtval = 0;
                    csrfile.mepc = pcReg;
                    excFlag = true;
#ifndef VIP_GEN
                    std::cout << "Unknown INST" << std::endl;
                    sc_stop();
                    wait(WAIT_TIME, SC_PS);
#endif
                }

            csrfile.mcause = updateMCAUSE(csrfile, excFlag);
            csrfile.mepc = updateMEPC(csrfile, pcReg, excFlag);
            pcReg = updatePC(csrfile, pcReg, excFlag);
            csrfile.mstatus = updateMSTATUS(csrfile, excFlag);
            excFlag = false;
            nextphase = Phases::fetch; // Fetch next instruction
        }
    }
}


EncType ISS_Interrupts::getEncType(unsigned int encodedInstr) const {
    if (OPCODE_FIELD(encodedInstr) == OPCODE_R) {
        return ENC_R;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_I) {
        return ENC_I_I;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_L) {
        return ENC_I_L;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_J) {
        return ENC_I_J;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_M) {
        return ENC_I_M;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_S) {
        return ENC_I_S;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_S) {
        return ENC_S;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_B) {
        return ENC_B;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_U1 || OPCODE_FIELD(encodedInstr) == OPCODE_U2) {
        return ENC_U;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_J) {
        return ENC_J;
    } else {
        return ENC_ERR;
    }
}


InstrType_Complete ISS_Interrupts::getInstrType(unsigned int encodedInstr) const {
    if (OPCODE_FIELD(encodedInstr) == OPCODE_R) {
        if (FUNCT3_FIELD(encodedInstr) == 0x00) {
            if (FUNCT7_FIELD(encodedInstr) == 0x00) {
                return INSTR_ADD;
            } else if (FUNCT7_FIELD(encodedInstr) == 0x20) {
                return INSTR_SUB;
            } else {
                return INSTR_UNKNOWN;
            }
        } else if (FUNCT3_FIELD(encodedInstr) == 0x01) {
            return INSTR_SLL;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x02) {
            return INSTR_SLT;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x03) {
            return INSTR_SLTU;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x04) {
            return INSTR_XOR;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x05) {
            if (FUNCT7_FIELD(encodedInstr) == 0x00) {
                return INSTR_SRL;
            } else if (FUNCT7_FIELD(encodedInstr) == 0x20) {
                return INSTR_SRA;
            } else {
                return INSTR_UNKNOWN;
            }
        } else if (FUNCT3_FIELD(encodedInstr) == 0x06) {
            return INSTR_OR;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x07) {
            return INSTR_AND;
        } else {
            return INSTR_UNKNOWN;
        }
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_I) {
        if (FUNCT3_FIELD(encodedInstr) == 0x00) {
            return INSTR_ADDI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x01) {
            return INSTR_SLLI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x02) {
            return INSTR_SLTI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x03) {
            return INSTR_SLTUI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x04) {
            return INSTR_XORI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x05) {
            if (FUNCT7_FIELD(encodedInstr) == 0x00) {
                return INSTR_SRLI;
            } else if (FUNCT7_FIELD(encodedInstr) == 0x20) {
                return INSTR_SRAI;
            } else {
                return INSTR_UNKNOWN;
            }
        } else if (FUNCT3_FIELD(encodedInstr) == 0x06) {
            return INSTR_ORI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x07) {
            return INSTR_ANDI;
        } else {
            return INSTR_UNKNOWN;
        }
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_L) {
        if (FUNCT3_FIELD(encodedInstr) == 0x00) {
            return INSTR_LB;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x01) {
            return INSTR_LH;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x02) {
            return INSTR_LW;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x04) {
            return INSTR_LBU;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x05) {
            return INSTR_LHU;
        } else {
            return INSTR_UNKNOWN;
        }
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_J) {
        return INSTR_JALR;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_M) {
        if (FUNCT3_FIELD(encodedInstr) == 0x00) {
            return INSTR_FENCE;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x01) {
            return INSTR_FENCEI;
        } else {
            return INSTR_UNKNOWN;
        }
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_I_S) {
        if (FUNCT3_FIELD(encodedInstr) == 0x00) {
            if(POS_IMM_I_FIELD(encodedInstr) == PRIVCODE_ECALL) {
                return INSTR_ECALL;
            } else if (POS_IMM_I_FIELD(encodedInstr) == PRIVCODE_EBREAK) {
                return INSTR_EBREAK;
            } else if (POS_IMM_I_FIELD(encodedInstr) == PRIVCODE_MRET) {
                return INSTR_MRET;
            } else {
                return INSTR_UNKNOWN;
            }
        } else if (FUNCT3_FIELD(encodedInstr) == 0x01) {
            return INSTR_CSRRW;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x02) {
            return INSTR_CSRRS;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x03) {
            return INSTR_CSRRC;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x05) {
            return INSTR_CSRRWI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x06) {
            return INSTR_CSRRSI;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x07) {
            return INSTR_CSRRCI;
        } else {
            return INSTR_UNKNOWN;
        }
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_S) {
        if (FUNCT3_FIELD(encodedInstr) == 0x00) {
            return INSTR_SB;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x01) {
            return INSTR_SH;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x02) {
            return INSTR_SW;
        } else {
            return INSTR_UNKNOWN;
        }
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_B) {
        if (FUNCT3_FIELD(encodedInstr) == 0x00) {
            return INSTR_BEQ;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x01) {
            return INSTR_BNE;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x04) {
            return INSTR_BLT;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x05) {
            return INSTR_BGE;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x06) {
            return INSTR_BLTU;
        } else if (FUNCT3_FIELD(encodedInstr) == 0x07) {
            return INSTR_BGEU;
        } else {
            return INSTR_UNKNOWN;
        }
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_U1) {
        return INSTR_LUI;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_U2) {
        return INSTR_AUIPC;
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_J) {
        return INSTR_JAL;
    } else {
        return INSTR_UNKNOWN;
    }
}

unsigned int ISS_Interrupts::getImmediate(unsigned int encodedInstr) const {
    if (OPCODE_FIELD(encodedInstr) == OPCODE_I_I ||
        OPCODE_FIELD(encodedInstr) == OPCODE_I_L ||
        OPCODE_FIELD(encodedInstr) == OPCODE_I_M ||
        OPCODE_FIELD(encodedInstr) == OPCODE_I_S ||
        OPCODE_FIELD(encodedInstr) == OPCODE_I_J) {
        if (SIGN_FIELD(encodedInstr) == 0)
            return POS_IMM_I_FIELD(encodedInstr);
        else
            return NEG_IMM_I_FIELD(encodedInstr);
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_S) {
        if (SIGN_FIELD(encodedInstr) == 0)
            return POS_IMM_S_FIELD(encodedInstr);
        else
            return NEG_IMM_S_FIELD(encodedInstr);
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_B) {
        if (SIGN_FIELD(encodedInstr) == 0)
            return POS_IMM_B_FIELD(encodedInstr);
        else
            return NEG_IMM_B_FIELD(encodedInstr);
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_U1 ||
               OPCODE_FIELD(encodedInstr) == OPCODE_U2) {
        return IMM_U_FIELD(encodedInstr);
    } else if (OPCODE_FIELD(encodedInstr) == OPCODE_J) {
        if (SIGN_FIELD(encodedInstr) == 0)
            return POS_IMM_J_FIELD(encodedInstr);
        else
            return NEG_IMM_J_FIELD(encodedInstr);
    } else {
        return 0;
    }
}


ALUfuncType ISS_Interrupts::getALUfunc(InstrType_Complete instr) const {
    if (instr == INSTR_ADD || instr == INSTR_ADDI ||
        instr == INSTR_LB || instr == INSTR_LH || instr == INSTR_LW || instr == INSTR_LBU || instr == INSTR_LHU ||
        instr == INSTR_SB || instr == INSTR_SH || instr == INSTR_SW ||
        instr == INSTR_AUIPC) {
        return ALU_ADD;
    } else if (instr == INSTR_SUB ||
               instr == INSTR_BEQ || instr == INSTR_BNE) {
        return ALU_SUB;
    } else if (instr == INSTR_SLL || instr == INSTR_SLLI) {
        return ALU_SLL;
    } else if (instr == INSTR_SLT || instr == INSTR_SLTI ||
               instr == INSTR_BLT || instr == INSTR_BGE) {
        return ALU_SLT;
    } else if (instr == INSTR_SLTU || instr == INSTR_SLTUI ||
               instr == INSTR_BLTU || instr == INSTR_BGEU) {
        return ALU_SLTU;
    } else if (instr == INSTR_XOR || instr == INSTR_XORI) {
        return ALU_XOR;
    } else if (instr == INSTR_SRL || instr == INSTR_SRLI) {
        return ALU_SRL;
    } else if (instr == INSTR_SRA || instr == INSTR_SRAI) {
        return ALU_SRA;
    } else if (instr == INSTR_OR || instr == INSTR_ORI || instr == INSTR_CSRRS || instr == INSTR_CSRRSI) {
        return ALU_OR;
    } else if (instr == INSTR_AND || instr == INSTR_ANDI) {
        return ALU_AND;
    } else if (instr == INSTR_JALR || instr == INSTR_JAL) {
        return ALU_X;
    } else if (instr == INSTR_LUI) {
        return ALU_COPY1;
    } else if (instr == INSTR_CSRRW || instr == INSTR_CSRRWI) {
        return ALU_CSRRW;
    } else if (instr == INSTR_CSRRC || instr == INSTR_CSRRCI) {
        return ALU_CSRRC;
    }
    else return ALU_X;
}


ME_MaskType ISS_Interrupts::getMemoryMask(InstrType_Complete instr) const {
    if (instr == INSTR_LW || instr == INSTR_SW) {
        return MT_W;
    } else if (instr == INSTR_LH || instr == INSTR_SH) {
        return MT_H;
    } else if (instr == INSTR_LB || instr == INSTR_SB) {
        return MT_B;
    } else if (instr == INSTR_LHU) {
        return MT_HU;
    } else if (instr == INSTR_LBU) {
        return MT_BU;
    } else return MT_X;
}

unsigned int ISS_Interrupts::getALUresult(ALUfuncType aluFunction, unsigned int operand1, unsigned int operand2) const {
    if (aluFunction == ALU_ADD) {
        return operand1 + operand2;
    } else if (aluFunction == ALU_SUB) {
        return operand1 - operand2;
    } else if (aluFunction == ALU_AND) {
        return operand1 & operand2;
    } else if (aluFunction == ALU_OR) {
        return operand1 | operand2;
    } else if (aluFunction == ALU_XOR) {
        return operand1 ^ operand2;
    } else if (aluFunction == ALU_SLT) {
        if (static_cast<int>(operand1) < static_cast<int>(operand2)) {
            return 1;
        } else {
            return 0;
        }
    } else if (aluFunction == ALU_SLTU) {
        if (operand1 < operand2) {
            return 1;
        } else {
            return 0;
        }
    } else if (aluFunction == ALU_SLL) {
        return operand1 << (operand2 & 0x1F);
    } else if (aluFunction == ALU_SRA) {
        return static_cast<unsigned int>(static_cast<int>(operand1) >> static_cast<int>(operand2 & 0x1F));
    } else if (aluFunction == ALU_SRL) {
        return operand1 >> (operand2 & 0x1F);
    } else if (aluFunction == ALU_COPY1) {
        return operand2;
    } if (aluFunction == ALU_CSRRW) {
        return operand1;
    } else if (aluFunction == ALU_CSRRC) {
        return operand2 & ((-operand1) - 1);
    } else {
        return 0;
    }
}


unsigned int ISS_Interrupts::getCSR(CSRfileType csrfile, unsigned int encodedInstr) const {
    if(POS_IMM_I_FIELD(encodedInstr) == MCSR_MSTATUS) {
        return csrfile.mstatus;
    } else if (POS_IMM_I_FIELD(encodedInstr) == MCSR_MIE) {
        return csrfile.mie;
    } else if (POS_IMM_I_FIELD(encodedInstr) == MCSR_MTVEC) {
        return csrfile.mtvec;
    } else if (POS_IMM_I_FIELD(encodedInstr) == MCSR_MEPC) {
        return csrfile.mepc;
    } else if (POS_IMM_I_FIELD(encodedInstr) == MCSR_MCAUSE) {
        return csrfile.mcause;
    } else if (POS_IMM_I_FIELD(encodedInstr) == MCSR_MSCRATCH) {
        return csrfile.mscratch;
    } else if (POS_IMM_I_FIELD(encodedInstr) == MCSR_MISA) {
        return csrfile.misa;
    } else if (POS_IMM_I_FIELD(encodedInstr) == MCSR_MTVAL) {
        return csrfile.mtval;
    } else {
        return 0;
    }
}


unsigned int ISS_Interrupts::updateMCAUSE(CSRfileType csrfile, bool excFlag) const {
    if (excFlag) { // exceptions
        return csrfile.mcause; // mcause should be given a value earlier with the instruction that caused the exception
    }
    else if (MSTATUS_MIE(csrfile.mstatus) != 0) {
#ifdef CoreTempSensor_ON
        if ((MTRAP_ML12I(csrfile.mie) != 0) && (MTRAP_ML12I(csrfile.mip) != 0)) {
            return 0x8000000C; // Mcause_ML12I
        } else
#endif
        if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) {
            return 0x8000000B; // Mcause_MEI
        } else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) {
            return 0x80000003; // Mcause_MSI
        } else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) {
            return 0x80000007; // Mcause_MTI
        } else {
            return csrfile.mcause;
        }
    } else {
        return csrfile.mcause;
    }
}


unsigned int ISS_Interrupts::updateMSTATUS(CSRfileType csrfile, bool excFlag) const {
    if (excFlag) { // exceptions
        return csrfile.mstatus & static_cast<unsigned int>(0xFFFFFFF7); //Setting MPIE(7) and Clearing MIE(3)
    }
    else if (MSTATUS_MIE(csrfile.mstatus) != 0) {
#ifdef CoreTempSensor_ON
        if ((MTRAP_ML12I(csrfile.mie) != 0) && (MTRAP_ML12I(csrfile.mip) != 0)) {
            return csrfile.mstatus & static_cast<unsigned int>(0xFFFFFFF7);
        } else
#endif
        if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) {
            return csrfile.mstatus & static_cast<unsigned int>(0xFFFFFFF7); // Setting MPIE(7) and Clearing MIE(3)
        } else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) {
            return csrfile.mstatus & static_cast<unsigned int>(0xFFFFFFF7);
        } else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) {
            return csrfile.mstatus & static_cast<unsigned int>(0xFFFFFFF7);
        } else {
            return csrfile.mstatus;
        }
    } else {
        return csrfile.mstatus;
    }
}


unsigned int ISS_Interrupts::updateMEPC(CSRfileType csrfile, unsigned int pcReg, bool excFlag) const {
    if (excFlag) { // mepc should be holding the address of the instruction that caused the exception from earlier
        return csrfile.mepc;
    }
    else if (MSTATUS_MIE(csrfile.mstatus) != 0) {
#ifdef CoreTempSensor_ON
        if ((MTRAP_ML12I(csrfile.mie) != 0) && (MTRAP_ML12I(csrfile.mip) != 0)) {
            return pcReg; // store the return from interrupt address
        } else
#endif
        if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) {
            return pcReg; // store the return from interrupt address
        } else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) {
            return pcReg; // store the return from interrupt address
        } else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) {
            return pcReg; // store the return from interrupt address
        } else {
            return csrfile.mepc;
        }
    } else {
        return csrfile.mepc;
    }
}


unsigned int ISS_Interrupts::updatePC(CSRfileType csrfile, unsigned int pcReg, bool excFlag) const {
    if (excFlag) { // exceptions
        return csrfile.mtvec;
    }
    else if (MSTATUS_MIE(csrfile.mstatus) != 0) {
#ifdef CoreTempSensor_ON
        if ((MTRAP_ML12I(csrfile.mie) != 0) && (MTRAP_ML12I(csrfile.mip) != 0)) {
            return csrfile.mtvec;
        } else
#endif
        if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) {
            return csrfile.mtvec;
        } else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) {
            return csrfile.mtvec;
        } else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) {
            return csrfile.mtvec;
        } else {
            return pcReg;
        }
    } else {
        return pcReg;
    }
}


unsigned int ISS_Interrupts::getBranchresult(unsigned int encodedInstr, unsigned int aluResult, unsigned int pcReg) const {
    if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_BEQ && aluResult == 0) {
        return pcReg + getImmediate(encodedInstr);
    } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_BNE && aluResult != 0) {
        return pcReg + getImmediate(encodedInstr);
    } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_BLT && aluResult == 1) {
        return pcReg + getImmediate(encodedInstr);
    } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_BGE && aluResult == 0) {
        return pcReg + getImmediate(encodedInstr);
    } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_BLTU && aluResult == 1) {
        return pcReg + getImmediate(encodedInstr);
    } else if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_BGEU && aluResult == 0) {
        return pcReg + getImmediate(encodedInstr);
    } else {
        return pcReg + 4;
    }
}


unsigned int ISS_Interrupts::getALUresult_U(unsigned int encodedInstr, unsigned int pcReg, unsigned int imm) const {
    if (getInstrType(encodedInstr) == InstrType_Complete::INSTR_LUI) {
        return imm;//getALUresult(ALU_COPY1, pcReg, imm); //Compute result
    } else { //INSTR_AUIPC
        return pcReg + imm;//getALUresult(ALU_ADD, pcReg, imm); //Compute result
    }
}

#endif //RISCV_ISS_Interrupts_H_
#pragma clang diagnostic pop
