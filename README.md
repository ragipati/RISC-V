![LUIBS EDA](logo.png)

This is a RISCV ISS techdemo of [LUBIS EDA](https://lubis-eda.com) to demonstrate how you can use SystemC descriptions to automatically generate formal properties for your verification. We established an executabel rv32i implementation in SystemC. The SystemC implementation is validated with standard RISCV tests as well as test cases implemented by LUBIS.  

![SystemCtoProperties](SystemCtoProperties.jpeg)

We developed a software that generate formal properties from abstract SystemC models. As part of this repo you'll find the generated properties. We used these properties to fully sign-off an RTL design, that is also part of this repo. 

All you need to check it out is a commercially available Formal Tool (OneSpin, VC Formal or JasperGold). 
We provided TCL script for all the tools to load and run the properties. 



**Content:**

* **ESL**: includes the Electronic system level description of the RISC-V core.
* **RTL**: RTL Design of the RISC-V Processor as well as Test Benches
* **SVA**: SVA properties to formally verify the correctness of the ISA module of the RTL design.

# Electronic System Level (ESL):

We created our system-level design for a RISC-V processor with exception and interrupt handling, as shown in the following figure. We will briefly describe the modules and sub-modules created for this design and the communication channels used between them.

![riscv_core](Test_Environment/Documentation/Core.png)

### ISA:
The main module of the core, it handles all the control and arithmetic executions in the core. It communicates with other modules through the following ports:

- **blocking_out <CUtoME_IF> toMemoryPort** used to send communication requests to the memory. Our RISC-V core follows a von Neumman architecture so there is only one memory for instructions and program data.
- **blocking_in<MEtoCU_IF> fromMemoryPort** used to receive data from the memory.
- **master_in<RegfileType> fromRegsPort** used for reading the register file.
- **master_out<RegfileWriteType> toRegsPort** used for writing back to the register file.
- **blocking_out<bool> isa_ecall_Port** used for triggering the EcallHandler when an ECALL instruction is executed. This communication has to be through a blocking channel in order to suspend execution of the ISA while the ECALL is being served.
- **blocking_in<bool> ecall_isa_Port** used for reading the status of the processor after ECALL. This status might trigger the end of simulation for system-level model when the environment call served is \textit{exit}.
- **master_in<unsigned int> mip_isa_Port** used for reading the status of the connected interrupt sources.


The sequence of communications with other modules differ depending on the instruction being executed. These instructions can be categorized according to their encryption type. Table \ref{table:riscv_isaComm} shows the sequence of communications for all possible encryption types following the pair of communication to read an instruction from memory.

|   |   |   |   |   |   |
|------|:------:|:------:|:------:|:------:|:------:|
| *R_Type* | RF_read | ... | ... | RF_write | ... | MIP_read |
| *B_Type* | RF_read | ... | ... | ... | ... | MIP_read |
| *S_Type* | RF_read | ... | **MEM_write** | ... | ... | MIP_read |
| *U_Type* | ... | ... | ... | RF_write | ... | MIP_read |
| *J_Type* | ... | ... | ... | RF_write | ... | MIP_read |o
| *I_I_Type* | RF_read | ... | ... | RF_write | ... | MIP_read |
| *I_L_Type* | RF_read | ... | **MEM_read** | RF_write | ... | MIP_read |
| *I_J_Type* | RF_read | ... | ... | ... | ... | MIP_read |
| *I_S_Type 1* | RF_read | ... | ... | RF_write | ... | MIP_read |
| *I_S_Type 2* | ... | ... | ... | RF_write | ... | MIP_read |
| *I_S_Type 3* (ECALL) | ... | ... | ... | ... | **EcallHandler** | MIP_read |
| *I_S_Type 3* | ... | ... | ... | ... | ... | MIP_read |


### Register File:
This module will always provide the content of the register file to the connected modules (ISA and EcallHandler) by using *slave_out* ports. It will also poll the input ports for writing requests. It has the following communication ports:

- **slave_in<RegfileWriteType> toRegsPort** used to read a write request from the ISA. This will overwrite the register specified by the request.
- **slave_out<RegfileType> fromRegsPort** used to provide the entire register file to the ISA.
- **slave_in<RegfileWriteType> ecall_reg_Port** used to read a write request from the EcallHandler to register (*x10*) in the register file.
- **slave_out<RegfileEcallType> reg_ecall_Port** used to provide registers (*x10*, *x11*, *x12* and *x17*) to the EcallHandler.

### Exception Handling:
An Exception refers to an unusual condition at runtime with an instruction in the current execution:

- **An instruction fetch misaligned exception** is generated on a taken branch or unconditional jump if the target address is not aligned to a four-byte boundary.
- **An instruction fetch misaligned exception** for all loads and stores is generated if the effective address is not naturally aligned for each data type(four byte boundary for 32 bit accesses and two-byte boundary for 16 bit accesses) .
- **Illegal instruction exception** is generated for the shift operations if imm[5]!=0(SHAMT[5]!=0) or if the fetched instruction is unknown.
- **Environment call exception** is generated on ECALL Instruction fetch and performs no other operation.
- **Breakpoint exception** is generated on EBREAK Instruction fetch and performs no other operation.

### Environment call handler:
This module is responsible for executing environment requests from the running program. These requests may include many types, but in our current implementation we are only supporting both (*printf* and *exit*). It has the following communication ports:

- **blocking_out<CUtoME_IF> toMemoryPort** used for requesting a read of memory content.
- **blocking_in<MEtoCU_IF> fromMemoryPort** used for reading data from the memory.
- **master_in<RegfileEcallType> reg_ecall_Port** used for reading registers (*x10*, *x11*, *x12* and *x17*) which should be holding the arguments and type of ecall request.
- **master_out<RegfileWriteType> ecall_reg_Port** used for writing back to the register file (specifically to *x10*, which should hold the result of an ecall request).
- **blocking_out<bool> ecall_isa_Port** used for returning to the ISA the status of the processor after handling the ecall.
- **blocking_in<bool> isa_ecall_Port** used for triggering the main process of the EcallHandler by the ISA.


### Pending machine interrupt handler:
This module is responsible for collecting the interrupt statuses (In our current implementation we are only supporting system, timer and external interrupts), and present them to the ISA as a single 32-bit value (`mip` register of CSRs) with each bit referring to a specific interrupt source. It has the following communication ports:

- **slave_out<unsigned int> mip_isa_Port** used for writing the 32-bit value to ISA.
- **master_in<bool> MSIP_port** used for reading the status of the system interrupt.
- **master_in<bool> MTIP_port** used for reading the status of the timer interrupt.
- **master_in<bool> MEIP_port** used for reading the status of the external interrupt.


### Core Local Interruptor (CLINT):
This module is used to generate software and timer Interrupts. It contains the RISC-V `msip`, `mtime` and `mtimecmp` memory mapped CSRs. The `msip` memory mapped CSR can be used to generate Machine Software Interrupts (MSIP). This register can be accessed by remote harts to provide machine-mode interprocessor interrupt (Not included in our current implementation). `mtime` and `mtimecmp` memory mapped CSRs can be used to generate Machine Timer Interrupts (MTIP). This interrupt is set when `mtime` exceed `mtimecmp` and it can be reset by writing a new bigger value to `mtimecmp`. This module has the following communication ports:

- **blocking_in<CUtoME_IF> COtoME_port** used for reading memory access requests from the Core.
- **blocking_out<MEtoCU_IF> MEtoCO_port** used for presenting the CLINT memory reply to the Core.
- **slave_out<bool> MSIP_port** used for presenting the MSIP status to the Core.
- **slave_out<bool> MTIP_port** used for presenting the MTIP status to the Core.

CLINT requires more than one thread to be running simultaneously and because currently SystemC-PPA doesn't support multiple threads, the internal of CLINT has been divided into sub-modules as shown in the figure above.


### Platform Level Interrupt Controller (PLIC):
This module is used to prioritize and distribute global interrupts and generate on their behalf what is called the External Interrupt (MEIP). For every added external interrupt source, a new gateway should be added to the *PLIC_Gateways* to handle the interrupt type (i.e. Level triggered or edge triggered interrupts) and required register and channels to *PLIC_Core* and *PLIC_Memory_Manager* follows as well. This module has the following communication ports:

- **blocking_in<CUtoME_IF> COtoME_port** used for reading memory access requests from the Core.
- **blocking_out<MEtoCU_IF> MEtoCO_port** used for presenting the PLIC memory reply to the Core.
- **slave_out<bool> MEIP_port** used for presenting the MEIP status to the Core.

PLIC requires more than one thread to be running simultaneously and because currently SystemC-PPA doesn't support multiple threads, the internal of PLIC has been divided into sub-modules as shown in figure above.


### CoreBus

This module is responsible for forwarding ISA and EcallHandler memory access requests and replies to the three connected memory mapped modules (CLINT, PLIC and Memory). It has the following communication ports:

- **blocking_in<CUtoME_IF> ecall_bus_Port** used for reading the EcallHandler memory access requests.
- **blocking_out<MEtoCU_IF> bus_ecall_Port** used for forwarding the memory reply to the EcallHandler.
- **blocking_in<CUtoME_IF> isa_bus_Port** used for reading the ISA memory access requests.
- **blocking_out<MEtoCU_IF> bus_isa_Port** used for forwarding the memory reply to the ISA.
- **blocking_out<CUtoME_IF> BUStoMEM_port** used for forwarding the Core memory access requests to Memory.
- **blocking_in<MEtoCU_IF> MEMtoBUS_port** used for reading the Memory reply.
- **blocking_out<CUtoME_IF> BUStoCLINT_port** used for forwarding the Core memory access requests to CLINT.
- **blocking_in<MEtoCU_IF> CLINTtoBUS_port** used for reading the CLINT reply.
- **blocking_out<CUtoME_IF> BUStoPLIC_port** used for forwarding the Core memory access requests to PLIC.
- **blocking_in<MEtoCU_IF> PLICtoBUS_port** used for reading the PLIC reply.

Adding new Peripherals will require adding two new communication ports to this module and manage the linking at the Core level.

### Core:
This module represents the Top model of the interconnection of all the previously mentioned modules and it forwards the Memory connections outward of the Core for consistency purposes with other RISCV implementations presented in this project. It has the following communication ports:

- **blocking_out<CUtoME_IF> COtoME_port** used for forwarding the memory access request out of Core.
- **blocking_in<MEtoCU_IF> MEtoCO_port** used for forwarding the memory reply to the internal modules of the Core.


# Register Transfer Level (RTL):

Most of the ESL modules of the RISC-V processor have been designed following the SystemC-Concilium methodology with the exception of some internal sub-modules in the CLINT and PLIC, which are considered as environmental components and not as parts of the main core or the RISC-V processor. From these ESL modules, a complete set of SVA properties have been generated for the ISA using the **LUBIS Concilium** tool. By following the Property Driven Development methodology, the RTL design of the RISC-V ISA was written. The generated properties are refined by modifying the macros with the RTL naming for all the ports and signals. As the fetching of an instruction takes two clock cycles in the RTL design a time refinement of two was needed for some of the properties. The tools used for checking the SVA properties are **OneSpin**,**VCFormal**,**JasperGold**.

The RTL directory includes the SystemVerilog implementation of the Core.

# SVA Properties

A set of tcl scripts are provided in the root folder that automatically loads the RTL design and runs the property checks based on the verification tool used. In the SVA folder, the properties are generated for each Verification tool i.e., Onespin, VCFormal and JasperGold using property generation by selecting the corresponding option in the LUBIS-Concilium plugin. To load a specific property set that is supported by one of the verification tool mentioned previously, use the respective tcl script.

Some known issues with JasperGold and VCFormal:

-For VCFormal: properties FETCH_DONE_to_FETCH_REQ, FETCH_DONE_to_FETCH_REQ_1, FETCH_DONE_to_LOAD_REQ, FETCH_DONE_to_STORE do not converge. Also it takes around 12 hours to prove the rest of the properties.

-For JasperGold all properties converge but takes around 7 hours to prove all properties.

For OneSpin there are no known issues and the properties prove in around 30 mins.

# Testing Environment:
The test environment is a cmake project with all the needed modules to compile it. First build the "Environment" target and then the "RISCV_Tests_All" target.
The ESL was verified using the unit tests from the riscv-tests repository. The tests are designed to check that the hardware it runs on is made according to the RISC-V specifications.
The relevant tests are precompiled, with exception to rv32mi-p-breakpoint, as this is not implemented.
The tests can be run from the Test_Environment folder with the test framework RISCV_Tests _All by running the run_riscv_tests script.

The test framework can be recompiled from the Environment folder if any changes are made to the core.
The tests write the value 1 to memory address 0x80001000 on successful completion of test, or fail code (TESTNUM << 1) | 1.
The memory.h file in the framework is modified to print out PASS or FAIL + failcode upon completion, and stop the simulation.


