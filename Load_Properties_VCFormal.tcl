# -------------------------------------------------
# Copyright(c) LUBIS EDA GmbH, All rights reserved
# Contact: contact@lubis-eda.com
# Author: Luis Rivas, Sandeep Ragipati
# -------------------------------------------------

set SCRIPT_LOCATION [file dirname [file normalize [info script]]];
set RTL_PATH $SCRIPT_LOCATION/RTL;
set PROP_PATH $SCRIPT_LOCATION/SVA;

# Witness generation
set_fml_var fml_witness_on true

set_grid_usage -type rsh=12
# design
analyze -format sverilog -vcs "${RTL_PATH}/globalTypes.sv
                               ${RTL_PATH}/ISS_Interrupts_types.sv
                               ${RTL_PATH}/ISS_Interrupts.sv"

# Lubis code
analyze -format sverilog -vcs "-assert svaext
            +incdir+${PROP_PATH}/SVA_Properties_VCFormal/+${RTL_PATH}
            ${PROP_PATH}/SVA_Properties_VCFormal/ISS_Interrupts_binding.sv
            -y ${PROP_PATH}/SVA_Properties_VCFormal/ +libext+.sv
            "

elaborate ISS_Interrupts -sva -vcs "-lca -sva_bind_enable ISS_Interrupts_binding"

create_clock clk -period 10
set_change_at -clock clk -posedge -default
create_reset rst -sense high
sim_run -stable
sim_save_reset
# Allowing rst as free primary input
remove_constant rst
check_fv -block
