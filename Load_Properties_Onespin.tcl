# -------------------------------------------------
# Copyright(c) LUBIS EDA GmbH, All rights reserved
# Contact: contact@lubis-eda.com
# Author: Luis Rivas, Sandeep Ragipati
# -------------------------------------------------

#------------------------------------------------------------------
# Setup mode
#------------------------------------------------------------------
set_mode setup
delete_design -both

#------------------
# Set path variable
#------------------
  set SCRIPT_LOCATION [file dirname [file normalize [info script]]]
  set RTL_PATH $SCRIPT_LOCATION/RTL

  set PROP_PATH $SCRIPT_LOCATION/SVA

#----------------------
# Read RTL files:
#----------------------
read_verilog -golden  -pragma_ignore {}  -version sv2012 $RTL_PATH/globalTypes.sv
read_verilog -golden  -pragma_ignore {}  -version sv2012 $RTL_PATH/ISS_Interrupts_types.sv
read_verilog -golden  -pragma_ignore {}  -version sv2012 $RTL_PATH/ISS_Interrupts.sv

#---------------------
# elaborate and compile
#---------------------
  elaborate -top ISS_Interrupts -verbose
  compile

#------------------------------------------------------------------
# MV mode
#------------------------------------------------------------------
set_mode mv
set_check_option -verbose
#set_disable_condition -high rst
check_consistency -category init

#--------------------------------
# check properties
#--------------------------------
#Load Onespin Past Style Regular Properties
read_sva -version sv2012 $PROP_PATH/SVA_Properties_OneSpin/ISS_Interrupts_binding.sv

time {check -all [get_checks]}
