//
// Created by tobias on 16.05.18.
//

#ifndef PROJECT_MEMORY_INTERFACES_H
#define PROJECT_MEMORY_INTERFACES_H

enum ME_MaskType {
    MT_B, MT_BU, MT_H, MT_HU, MT_W, MT_X
};

enum ME_AccessType {
    ME_RD, ME_WR, ME_X
};

struct CUtoME_IF {
    ME_AccessType req;
    ME_MaskType mask;
    unsigned int addrIn; //FIXME: Changed from unsigned int
    unsigned int dataIn;
};

struct MEtoCU_IF {
    unsigned int loadedData;
};

#endif //PROJECT_MEMORY_INTERFACES_H