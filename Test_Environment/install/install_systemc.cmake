# Encapsulates building SystemC as an External Project.


if (USE_SYSTEM_SYSTEMC)
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMakeModules/")
    find_package(SystemC 2.3.3 REQUIRED)
else ()
    ExternalProject_Add(SYSTEMC
            # Location for external project with standard folder structure. Distinct by version
            PREFIX ${CMAKE_EXTERNAL_PROJECT_DIR}/systemc
            # Download the project from git via versioned tag. Checkout only the tag. Be verbose.
            GIT_REPOSITORY https://github.com/accellera-official/systemc.git
            GIT_TAG ${SYSTEMC_VERSION}
            GIT_SHALLOW TRUE
            GIT_PROGRESS TRUE
            GIT_CONFIG advice.detachedHead=false

            # Do not update the project, as we intentionally checked out a specific version.
            UPDATE_COMMAND ""

            CMAKE_ARGS
            -DCMAKE_CXX_STANDARD=17
            -DCMAKE_BUILD_TYPE=Release
            -DCMAKE_INSTALL_PREFIX=${CMAKE_SOURCE_DIR}
            )

    set(SYSTEMC_INCLUDE_DIR ${CMAKE_EXTERNAL_PROJECT_DIR}/systemc/src/SYSTEMC/src)
endif ()