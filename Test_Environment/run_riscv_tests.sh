#!/bin/bash
export SYSTEMC_DISABLE_COPYRIGHT_MESSAGE=1
export SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#Running RISCV Official Tests
echo -e "\e[30;48;5;82mRunning RISCV Official Tests\e[0m";
for f in ${SCRIPT_DIR}/Tests/riscv-tests/rv32ui-p/*;
do
    "${SCRIPT_DIR}/bin/RISCV_Tests_All" "$f"
done

for f in ${SCRIPT_DIR}/Tests/riscv-tests/rv32mi-p/*;
do
    "${SCRIPT_DIR}/bin/RISCV_Tests_All" "$f"
done

echo -e "\e[30;48;5;82mDone!\e[0m";

