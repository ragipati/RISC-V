// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 23.06.2022 at 09:16
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


`ifndef ISS_INTERRUPTS_BINDING
`define ISS_INTERRUPTS_BINDING


// Module binding
`include "ISS_Interrupts_assertions.sv"

module ISS_Interrupts_binding();

bind ISS_Interrupts ISS_Interrupts_verification inst(.*, .reset(`rst));

endmodule

`endif
