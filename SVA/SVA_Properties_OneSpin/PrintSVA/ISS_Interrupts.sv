// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 17.08.2022 at 11:10
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


`include "../../constraints.sv"


import global_package::*;


// Enumeration types

typedef enum { execute, fetch } e_Phases;


// Functions

function e_ALUfuncType getALUfunc(e_InstrType_Complete instr);
  if ((((((((((((instr == INSTR_ADD) || (instr == INSTR_ADDI)) || (instr == INSTR_LB)) || (instr == INSTR_LH)) || (instr == INSTR_LW)) || (instr == INSTR_LBU)) || (instr == INSTR_LHU)) || (instr == INSTR_SB)) || (instr == INSTR_SH)) || (instr == INSTR_SW)) || (instr == INSTR_AUIPC)))
    return ALU_ADD;
  else if ((((instr == INSTR_SUB) || (instr == INSTR_BEQ)) || (instr == INSTR_BNE)))
    return ALU_SUB;
  else if (((instr == INSTR_SLL) || (instr == INSTR_SLLI)))
    return ALU_SLL;
  else if (((((instr == INSTR_SLT) || (instr == INSTR_SLTI)) || (instr == INSTR_BLT)) || (instr == INSTR_BGE)))
    return ALU_SLT;
  else if (((((instr == INSTR_SLTU) || (instr == INSTR_SLTUI)) || (instr == INSTR_BLTU)) || (instr == INSTR_BGEU)))
    return ALU_SLTU;
  else if (((instr == INSTR_XOR) || (instr == INSTR_XORI)))
    return ALU_XOR;
  else if (((instr == INSTR_SRL) || (instr == INSTR_SRLI)))
    return ALU_SRL;
  else if (((instr == INSTR_SRA) || (instr == INSTR_SRAI)))
    return ALU_SRA;
  else if (((((instr == INSTR_OR) || (instr == INSTR_ORI)) || (instr == INSTR_CSRRS)) || (instr == INSTR_CSRRSI)))
    return ALU_OR;
  else if (((instr == INSTR_AND) || (instr == INSTR_ANDI)))
    return ALU_AND;
  else if (((instr == INSTR_JALR) || (instr == INSTR_JAL)))
    return ALU_X;
  else if ((instr == INSTR_LUI))
    return ALU_COPY1;
  else if (((instr == INSTR_CSRRW) || (instr == INSTR_CSRRWI)))
    return ALU_CSRRW;
  else if (((instr == INSTR_CSRRC) || (instr == INSTR_CSRRCI)))
    return ALU_CSRRC;
  else
    return ALU_X;
endfunction

function bit[31:0] getALUresult(e_ALUfuncType aluFunction, bit[31:0] operand1, bit[31:0] operand2);
  if ((aluFunction == ALU_ADD))
    return (operand1 + operand2);
  else if ((aluFunction == ALU_SUB))
    return (operand1 - operand2);
  else if ((aluFunction == ALU_AND))
    return (operand1 & operand2);
  else if ((aluFunction == ALU_OR))
    return (operand1 | operand2);
  else if ((aluFunction == ALU_XOR))
    return (operand1 ^ operand2);
  else if ((aluFunction == ALU_SLT) && (signed'(32'(operand1)) < signed'(32'(operand2))))
    return 1;
  else if ((aluFunction == ALU_SLT))
    return 0;
  else if ((aluFunction == ALU_SLTU) && (operand1 < operand2))
    return 1;
  else if ((aluFunction == ALU_SLTU))
    return 0;
  else if ((aluFunction == ALU_SLL))
    return (operand1 << (operand2 & 'h1F));
  else if ((aluFunction == ALU_SRA))
    return unsigned'(32'((signed'(32'(operand1)) >>> signed'(32'((operand2 & 'h1F))))));
  else if ((aluFunction == ALU_SRL))
    return (operand1 >> (operand2 & 'h1F));
  else if ((aluFunction == ALU_COPY1))
    return operand2;
  else if ((aluFunction == ALU_CSRRW))
    return operand1;
  else if ((aluFunction == ALU_CSRRC))
    return (operand2 & ((operand1 * 4294967295) - 1));
  else
    return 0;
endfunction

function bit[31:0] getALUresult_U(bit[31:0] encodedInstr, bit[31:0] pcReg, bit[31:0] imm);
  if ((getInstrType(encodedInstr) == INSTR_LUI))
    return imm;
  else
    return (pcReg + imm);
endfunction

function bit[31:0] getBranchresult(bit[31:0] encodedInstr, bit[31:0] aluResult, bit[31:0] pcReg);
  if (((getInstrType(encodedInstr) == INSTR_BEQ) && (aluResult == 0)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BNE) && (aluResult != 0)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BLT) && (aluResult == 1)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BGE) && (aluResult == 0)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BLTU) && (aluResult == 1)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BGEU) && (aluResult == 0)))
    return (pcReg + getImmediate(encodedInstr));
  else
    return (pcReg + 4);
endfunction

function bit[31:0] getCSR(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit[31:0] encodedInstr);
  if (((encodedInstr >> 20) == 768))
    return csrfile_mstatus;
  else if (((encodedInstr >> 20) == 772))
    return csrfile_mie;
  else if (((encodedInstr >> 20) == 773))
    return csrfile_mtvec;
  else if (((encodedInstr >> 20) == 833))
    return csrfile_mepc;
  else if (((encodedInstr >> 20) == 834))
    return csrfile_mcause;
  else if (((encodedInstr >> 20) == 832))
    return csrfile_mscratch;
  else if (((encodedInstr >> 20) == 769))
    return csrfile_misa;
  else if (((encodedInstr >> 20) == 835))
    return csrfile_mtval;
  else
    return 0;
endfunction

function e_EncType getEncType(bit[31:0] encodedInstr);
  if (((encodedInstr & 127) == 51))
    return ENC_R;
  else if (((encodedInstr & 127) == 19))
    return ENC_I_I;
  else if (((encodedInstr & 127) == 3))
    return ENC_I_L;
  else if (((encodedInstr & 127) == 103))
    return ENC_I_J;
  else if (((encodedInstr & 127) == 15))
    return ENC_I_M;
  else if (((encodedInstr & 127) == 115))
    return ENC_I_S;
  else if (((encodedInstr & 127) == 35))
    return ENC_S;
  else if (((encodedInstr & 127) == 99))
    return ENC_B;
  else if ((((encodedInstr & 127) == 55) || ((encodedInstr & 127) == 23)))
    return ENC_U;
  else if (((encodedInstr & 127) == 111))
    return ENC_J;
  else
    return ENC_ERR;
endfunction

function bit[31:0] getImmediate(bit[31:0] encodedInstr);
  if (((((((encodedInstr & 127) == 19) || ((encodedInstr & 127) == 3)) || ((encodedInstr & 127) == 15)) || ((encodedInstr & 127) == 115)) || ((encodedInstr & 127) == 103)) && (((encodedInstr >> 31) & 1) == 0))
    return (encodedInstr >> 20);
  else if (((((((encodedInstr & 127) == 19) || ((encodedInstr & 127) == 3)) || ((encodedInstr & 127) == 15)) || ((encodedInstr & 127) == 115)) || ((encodedInstr & 127) == 103)))
    return (unsigned'(32'(-4096)) | (encodedInstr >> 20));
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 31) & 1) == 0))
    return (((encodedInstr >> 20) & 4064) | ((encodedInstr >> 7) & 31));
  else if (((encodedInstr & 127) == 35))
    return (unsigned'(32'(-4096)) | (((encodedInstr >> 20) & 4064) | ((encodedInstr >> 7) & 31)));
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 31) & 1) == 0))
    return ((((encodedInstr << 4) & 2048) | ((encodedInstr >> 20) & 2016)) | ((encodedInstr >> 7) & 30));
  else if (((encodedInstr & 127) == 99))
    return (unsigned'(32'(-4096)) | ((((encodedInstr << 4) & 2048) | ((encodedInstr >> 20) & 2016)) | ((encodedInstr >> 7) & 30)));
  else if ((((encodedInstr & 127) == 55) || ((encodedInstr & 127) == 23)))
    return (encodedInstr & unsigned'(32'(-4096)));
  else if (((encodedInstr & 127) == 111) && (((encodedInstr >> 31) & 1) == 0))
    return (((encodedInstr & 1044480) | ((encodedInstr >> 9) & 2048)) | ((encodedInstr >> 20) & 2046));
  else if (((encodedInstr & 127) == 111))
    return (unsigned'(32'(-1048576)) | (((encodedInstr & 1044480) | ((encodedInstr >> 9) & 2048)) | ((encodedInstr >> 20) & 2046)));
  else
    return 0;
endfunction

function e_InstrType_Complete getInstrType(bit[31:0] encodedInstr);
  if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h0) && (((encodedInstr >> 25) & 127) == 'h0))
    return INSTR_ADD;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h0) && (((encodedInstr >> 25) & 127) == 'h20))
    return INSTR_SUB;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h0))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h1))
    return INSTR_SLL;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h2))
    return INSTR_SLT;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h3))
    return INSTR_SLTU;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h4))
    return INSTR_XOR;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h5) && (((encodedInstr >> 25) & 127) == 'h0))
    return INSTR_SRL;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h5) && (((encodedInstr >> 25) & 127) == 'h20))
    return INSTR_SRA;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h5))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h6))
    return INSTR_OR;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 'h7))
    return INSTR_AND;
  else if (((encodedInstr & 127) == 51))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h0))
    return INSTR_ADDI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h1))
    return INSTR_SLLI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h2))
    return INSTR_SLTI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h3))
    return INSTR_SLTUI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h4))
    return INSTR_XORI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h5) && (((encodedInstr >> 25) & 127) == 'h0))
    return INSTR_SRLI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h5) && (((encodedInstr >> 25) & 127) == 'h20))
    return INSTR_SRAI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h5))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h6))
    return INSTR_ORI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 'h7))
    return INSTR_ANDI;
  else if (((encodedInstr & 127) == 19))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 'h0))
    return INSTR_LB;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 'h1))
    return INSTR_LH;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 'h2))
    return INSTR_LW;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 'h4))
    return INSTR_LBU;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 'h5))
    return INSTR_LHU;
  else if (((encodedInstr & 127) == 3))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 103))
    return INSTR_JALR;
  else if (((encodedInstr & 127) == 15) && (((encodedInstr >> 12) & 7) == 'h0))
    return INSTR_FENCE;
  else if (((encodedInstr & 127) == 15) && (((encodedInstr >> 12) & 7) == 'h1))
    return INSTR_FENCEI;
  else if (((encodedInstr & 127) == 15))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h0) && ((encodedInstr >> 20) == 0))
    return INSTR_ECALL;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h0) && ((encodedInstr >> 20) == 1))
    return INSTR_EBREAK;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h0) && ((encodedInstr >> 20) == 770))
    return INSTR_MRET;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h0))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h1))
    return INSTR_CSRRW;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h2))
    return INSTR_CSRRS;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h3))
    return INSTR_CSRRC;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h5))
    return INSTR_CSRRWI;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h6))
    return INSTR_CSRRSI;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 'h7))
    return INSTR_CSRRCI;
  else if (((encodedInstr & 127) == 115))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 12) & 7) == 'h0))
    return INSTR_SB;
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 12) & 7) == 'h1))
    return INSTR_SH;
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 12) & 7) == 'h2))
    return INSTR_SW;
  else if (((encodedInstr & 127) == 35))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 'h0))
    return INSTR_BEQ;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 'h1))
    return INSTR_BNE;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 'h4))
    return INSTR_BLT;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 'h5))
    return INSTR_BGE;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 'h6))
    return INSTR_BLTU;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 'h7))
    return INSTR_BGEU;
  else if (((encodedInstr & 127) == 99))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 55))
    return INSTR_LUI;
  else if (((encodedInstr & 127) == 23))
    return INSTR_AUIPC;
  else if (((encodedInstr & 127) == 111))
    return INSTR_JAL;
  else
    return INSTR_UNKNOWN;
endfunction

function e_ME_MaskType getMemoryMask(e_InstrType_Complete instr);
  if (((instr == INSTR_LW) || (instr == INSTR_SW)))
    return MT_W;
  else if (((instr == INSTR_LH) || (instr == INSTR_SH)))
    return MT_H;
  else if (((instr == INSTR_LB) || (instr == INSTR_SB)))
    return MT_B;
  else if ((instr == INSTR_LHU))
    return MT_HU;
  else if ((instr == INSTR_LBU))
    return MT_BU;
  else
    return MT_X;
endfunction

function bit[31:0] updateMCAUSE(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit excFlag);
  if (excFlag)
    return csrfile_mcause;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return 'h8000000B;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return 'h80000003;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return 'h80000007;
  else if (((csrfile_mstatus & 8) != 0))
    return csrfile_mcause;
  else
    return csrfile_mcause;
endfunction

function bit[31:0] updateMEPC(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit[31:0] pcReg, bit excFlag);
  if (excFlag)
    return csrfile_mepc;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return pcReg;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return pcReg;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return pcReg;
  else if (((csrfile_mstatus & 8) != 0))
    return csrfile_mepc;
  else
    return csrfile_mepc;
endfunction

function bit[31:0] updateMSTATUS(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit excFlag);
  if (excFlag)
    return (csrfile_mstatus & unsigned'(32'('hFFFFFFF7)));
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return (csrfile_mstatus & unsigned'(32'('hFFFFFFF7)));
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return (csrfile_mstatus & unsigned'(32'('hFFFFFFF7)));
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return (csrfile_mstatus & unsigned'(32'('hFFFFFFF7)));
  else if (((csrfile_mstatus & 8) != 0))
    return csrfile_mstatus;
  else
    return csrfile_mstatus;
endfunction

function bit[31:0] updatePC(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit[31:0] pcReg, bit excFlag);
  if (excFlag)
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0))
    return pcReg;
  else
    return pcReg;
endfunction


sequence hold(l, e);
  (l===e, l=e);
endsequence


module ISS_Interrupts_verification(
  input bit rst,
  input bit clk,
  input bit ecall_isa_Port_sig,
  input bit[31:0] fromMemoryPort_sig_loadedData,
  input a_u_32_32 fromRegsPort_sig,
  input bit[31:0] fromRegsPort_sig_0,
  input bit[31:0] fromRegsPort_sig_1,
  input bit[31:0] fromRegsPort_sig_2,
  input bit[31:0] fromRegsPort_sig_3,
  input bit[31:0] fromRegsPort_sig_4,
  input bit[31:0] fromRegsPort_sig_5,
  input bit[31:0] fromRegsPort_sig_6,
  input bit[31:0] fromRegsPort_sig_7,
  input bit[31:0] fromRegsPort_sig_8,
  input bit[31:0] fromRegsPort_sig_9,
  input bit[31:0] fromRegsPort_sig_10,
  input bit[31:0] fromRegsPort_sig_11,
  input bit[31:0] fromRegsPort_sig_12,
  input bit[31:0] fromRegsPort_sig_13,
  input bit[31:0] fromRegsPort_sig_14,
  input bit[31:0] fromRegsPort_sig_15,
  input bit[31:0] fromRegsPort_sig_16,
  input bit[31:0] fromRegsPort_sig_17,
  input bit[31:0] fromRegsPort_sig_18,
  input bit[31:0] fromRegsPort_sig_19,
  input bit[31:0] fromRegsPort_sig_20,
  input bit[31:0] fromRegsPort_sig_21,
  input bit[31:0] fromRegsPort_sig_22,
  input bit[31:0] fromRegsPort_sig_23,
  input bit[31:0] fromRegsPort_sig_24,
  input bit[31:0] fromRegsPort_sig_25,
  input bit[31:0] fromRegsPort_sig_26,
  input bit[31:0] fromRegsPort_sig_27,
  input bit[31:0] fromRegsPort_sig_28,
  input bit[31:0] fromRegsPort_sig_29,
  input bit[31:0] fromRegsPort_sig_30,
  input bit[31:0] fromRegsPort_sig_31,
  input bit isa_ecall_Port_sig,
  input bit[31:0] mip_isa_Port_sig,
  input bit[31:0] toMemoryPort_sig_addrIn,
  input bit[31:0] toMemoryPort_sig_dataIn,
  input e_ME_MaskType toMemoryPort_sig_mask,
  input e_ME_AccessType toMemoryPort_sig_req,
  input bit[31:0] toRegsPort_sig_dst,
  input bit[31:0] toRegsPort_sig_dstData,
  input bit ecall_isa_Port_sync,
  input bit fromMemoryPort_sync,
  input bit isa_ecall_Port_sync,
  input bit toMemoryPort_sync,
  input bit ecall_isa_Port_notify,
  input bit fromMemoryPort_notify,
  input bit isa_ecall_Port_notify,
  input bit toMemoryPort_notify,
  input bit toRegsPort_notify,
  input bit[31:0] csrfile_mcause,
  input bit[31:0] csrfile_mepc,
  input bit[31:0] csrfile_mie,
  input bit[31:0] csrfile_mip,
  input bit[31:0] csrfile_misa,
  input bit[31:0] csrfile_mscratch,
  input bit[31:0] csrfile_mstatus,
  input bit[31:0] csrfile_mtval,
  input bit[31:0] csrfile_mtvec,
  input bit excFlag,
  input bit[31:0] pcReg,
  input e_Phases phase,
  input bit[31:0] regfileWrite_dst,
  input bit[31:0] regfileWrite_dstData,
  input bit FETCH_REQ,
  input bit FETCH_DONE,
  input bit STORE,
  input bit LOAD_REQ,
  input bit LOAD_DONE,
  input bit ECALL_REQ,
  input bit ECALL_DONE
);


default clocking default_clk @(posedge clk); endclocking


sequence reset_sequence;
  rst ##1 !rst;
endsequence


reset_a: assert property (reset_p);
property reset_p;
  reset_sequence |->
  FETCH_REQ &&
  csrfile_mcause == 0 &&
  csrfile_mepc == 0 &&
  csrfile_mie == 0 &&
  csrfile_mip == 0 &&
  csrfile_misa == 0 &&
  csrfile_mscratch == 0 &&
  csrfile_mstatus == 0 &&
  csrfile_mtval == 0 &&
  csrfile_mtvec == 0 &&
  excFlag == 0 &&
  pcReg == 0 &&
  phase == fetch &&
  regfileWrite_dst == 0 &&
  regfileWrite_dstData == 0 &&
  toMemoryPort_sig_addrIn == 0 &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  ecall_isa_Port_notify == 0 &&
  fromMemoryPort_notify == 0 &&
  isa_ecall_Port_notify == 0 &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 0;
endproperty


ECALL_DONE_to_FETCH_REQ_a: assert property (disable iff(rst) ECALL_DONE_to_FETCH_REQ_p);
property ECALL_DONE_to_FETCH_REQ_p;
  ##1
  ECALL_DONE &&
  ecall_isa_Port_sync
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0)) and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##1
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(pcReg, 1), $past(excFlag, 1)) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(pcReg, 1), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(pcReg, 1), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(pcReg, 1), $past(excFlag, 1)) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(pcReg, 1), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(pcReg, 1), $past(excFlag, 1)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


ECALL_REQ_to_ECALL_DONE_a: assert property (disable iff(rst) ECALL_REQ_to_ECALL_DONE_p);
property ECALL_REQ_to_ECALL_DONE_p;
  ##1
  ECALL_REQ &&
  isa_ecall_Port_sync
|->
  ($past(ecall_isa_Port_notify == 0, 0)) and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##1
  ECALL_DONE &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  ecall_isa_Port_notify == 1;
endproperty


FETCH_DONE_to_ECALL_REQ_a: assert property (disable iff(rst) FETCH_DONE_to_ECALL_REQ_p);
property FETCH_DONE_to_ECALL_REQ_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_ECALL)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0))[*3] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  ECALL_REQ &&
  csrfile_mcause == 11 &&
  csrfile_mepc == (4 + $past(pcReg, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == $past(csrfile_mstatus, 2) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 1 &&
  isa_ecall_Port_sig == 1 &&
  pcReg == $past(pcReg, 2) &&
  phase == execute &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  isa_ecall_Port_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_p);
property FETCH_DONE_to_FETCH_REQ_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_R)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_1_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_1_p);
property FETCH_DONE_to_FETCH_REQ_1_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_B) &&
  ((getBranchresult(fromMemoryPort_sig_loadedData, getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 20) & 31)]), pcReg) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_10_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_10_p);
property FETCH_DONE_to_FETCH_REQ_10_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_J) &&
  ((((fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)] + getImmediate(fromMemoryPort_sig_loadedData)) & 'hFFFFFFFE) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_11_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_11_p);
property FETCH_DONE_to_FETCH_REQ_11_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_J) &&
  !((((fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)] + getImmediate(fromMemoryPort_sig_loadedData)) & 'hFFFFFFFE) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == (4 + $past(pcReg, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ((fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFE), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == (4 + $past(pcReg, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_12_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_12_p);
property FETCH_DONE_to_FETCH_REQ_12_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_M)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_13_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_13_p);
property FETCH_DONE_to_FETCH_REQ_13_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 768) &&
  (((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getCSR(csrfile_mcause, csrfile_mepc, csrfile_mie, mip_isa_Port_sig, csrfile_misa, csrfile_mscratch, csrfile_mstatus, csrfile_mtval, csrfile_mtvec, fromMemoryPort_sig_loadedData)) >> 11) & 3) == 3)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_14_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_14_p);
property FETCH_DONE_to_FETCH_REQ_14_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 768) &&
  !(((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getCSR(csrfile_mcause, csrfile_mepc, csrfile_mie, mip_isa_Port_sig, csrfile_misa, csrfile_mscratch, csrfile_mstatus, csrfile_mtval, csrfile_mtvec, fromMemoryPort_sig_loadedData)) >> 11) & 3) == 3)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_15_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_15_p);
property FETCH_DONE_to_FETCH_REQ_15_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 772)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_16_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_16_p);
property FETCH_DONE_to_FETCH_REQ_16_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 773)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_17_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_17_p);
property FETCH_DONE_to_FETCH_REQ_17_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 833)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_18_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_18_p);
property FETCH_DONE_to_FETCH_REQ_18_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 834)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_19_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_19_p);
property FETCH_DONE_to_FETCH_REQ_19_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 832)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_2_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_2_p);
property FETCH_DONE_to_FETCH_REQ_2_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_B) &&
  !((getBranchresult(fromMemoryPort_sig_loadedData, getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 20) & 31)]), pcReg) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), getBranchresult($past(fromMemoryPort_sig_loadedData, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)]), $past(pcReg, 2)), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_20_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_20_p);
property FETCH_DONE_to_FETCH_REQ_20_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 769)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == 'h40000080 &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_21_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_21_p);
property FETCH_DONE_to_FETCH_REQ_21_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 835)
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_22_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_22_p);
property FETCH_DONE_to_FETCH_REQ_22_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS)) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 768) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 772) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 773) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 833) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 834) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 832) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 769) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 835)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0)) and
  ($past(toRegsPort_notify == 0, 0)) and
  ##1
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(mip_isa_Port_sig, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_23_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_23_p);
property FETCH_DONE_to_FETCH_REQ_23_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 768) &&
  (((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), ((fromMemoryPort_sig_loadedData >> 15) & 31), getCSR(csrfile_mcause, csrfile_mepc, csrfile_mie, mip_isa_Port_sig, csrfile_misa, csrfile_mscratch, csrfile_mstatus, csrfile_mtval, csrfile_mtvec, fromMemoryPort_sig_loadedData)) >> 11) & 3) == 3)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_24_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_24_p);
property FETCH_DONE_to_FETCH_REQ_24_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 768) &&
  !(((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), ((fromMemoryPort_sig_loadedData >> 15) & 31), getCSR(csrfile_mcause, csrfile_mepc, csrfile_mie, mip_isa_Port_sig, csrfile_misa, csrfile_mscratch, csrfile_mstatus, csrfile_mtval, csrfile_mtvec, fromMemoryPort_sig_loadedData)) >> 11) & 3) == 3)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_25_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_25_p);
property FETCH_DONE_to_FETCH_REQ_25_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 772)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_26_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_26_p);
property FETCH_DONE_to_FETCH_REQ_26_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 773)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) & 'hFFFFFFFC), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_27_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_27_p);
property FETCH_DONE_to_FETCH_REQ_27_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 833)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0)) and
  ($past(toRegsPort_notify == 0, 0)) and
  ##1
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(mip_isa_Port_sig, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) & 'hFFFFFFFC), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_28_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_28_p);
property FETCH_DONE_to_FETCH_REQ_28_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 834)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0)) and
  ($past(toRegsPort_notify == 0, 0)) and
  ##1
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(mip_isa_Port_sig, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_29_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_29_p);
property FETCH_DONE_to_FETCH_REQ_29_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 832)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0)) and
  ($past(toRegsPort_notify == 0, 0)) and
  ##1
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(mip_isa_Port_sig, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 1))), (($past(fromMemoryPort_sig_loadedData, 1) >> 15) & 31), getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1))), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 1) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(mip_isa_Port_sig, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(fromMemoryPort_sig_loadedData, 1)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_3_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_3_p);
property FETCH_DONE_to_FETCH_REQ_3_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SH) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(6, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_30_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_30_p);
property FETCH_DONE_to_FETCH_REQ_30_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 769)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == 'h40000080 &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), 'h40000080, $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_31_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_31_p);
property FETCH_DONE_to_FETCH_REQ_31_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData) == 835)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), (($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31), getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_32_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_32_p);
property FETCH_DONE_to_FETCH_REQ_32_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI)) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 768) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 772) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 773) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 833) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 834) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 832) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 769) &&
  !(getImmediate(fromMemoryPort_sig_loadedData) == 835)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getCSR($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(fromMemoryPort_sig_loadedData, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_33_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_33_p);
property FETCH_DONE_to_FETCH_REQ_33_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_EBREAK)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(3, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_34_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_34_p);
property FETCH_DONE_to_FETCH_REQ_34_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_MRET)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(csrfile_mepc, 2), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(csrfile_mepc, 2), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(csrfile_mepc, 2), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(csrfile_mepc, 2), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(csrfile_mepc, 2), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), ($past(csrfile_mstatus, 2) | 'h8), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(csrfile_mepc, 2), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_35_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_35_p);
property FETCH_DONE_to_FETCH_REQ_35_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_S) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRW) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRS) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRC) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRWI) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRSI) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_CSRRCI) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_ECALL) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_EBREAK) &&
  !(getInstrType(fromMemoryPort_sig_loadedData) == INSTR_MRET)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_36_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_36_p);
property FETCH_DONE_to_FETCH_REQ_36_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_ERR)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == 0 &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_4_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_4_p);
property FETCH_DONE_to_FETCH_REQ_4_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_U)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getALUresult_U($past(fromMemoryPort_sig_loadedData, 2), $past(pcReg, 2), getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getALUresult_U($past(fromMemoryPort_sig_loadedData, 2), $past(pcReg, 2), getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_5_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_5_p);
property FETCH_DONE_to_FETCH_REQ_5_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_J) &&
  (((pcReg + getImmediate(fromMemoryPort_sig_loadedData)) % 4) != 0)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_6_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_6_p);
property FETCH_DONE_to_FETCH_REQ_6_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_J) &&
  !(((pcReg + getImmediate(fromMemoryPort_sig_loadedData)) % 4) != 0)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == (4 + $past(pcReg, 2)) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == (4 + $past(pcReg, 2)) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_7_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_7_p);
property FETCH_DONE_to_FETCH_REQ_7_p;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_I) &&
  ((((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SLLI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SRLI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SRAI)) && ((fromMemoryPort_sig_loadedData & 33554432) != 0))
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == 0 &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), 0, $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_8_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_8_p);
property FETCH_DONE_to_FETCH_REQ_8_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_I) &&
  !((((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SLLI) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SRLI)) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SRAI)) && ((fromMemoryPort_sig_loadedData & 33554432) != 0))
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  phase == fetch &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 2), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), $past(excFlag, 2)), $past(csrfile_mepc, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), $past(csrfile_mtval, 2), $past(csrfile_mtvec, 2), ($past(pcReg, 2) + 4), $past(excFlag, 2)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  toRegsPort_sig_dstData == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


FETCH_DONE_to_FETCH_REQ_9_a: assert property (disable iff(rst) FETCH_DONE_to_FETCH_REQ_9_p);
property FETCH_DONE_to_FETCH_REQ_9_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_L) &&
  ((((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_LH) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_LHU)) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_LW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mepc == updateMEPC(updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1) &&
  csrfile_mtval == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), updateMEPC(updateMCAUSE(4, $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), 1), $past(pcReg, 2), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1), $past(csrfile_mie, 2), $past(mip_isa_Port_sig, 2), $past(csrfile_misa, 2), $past(csrfile_mscratch, 2), $past(csrfile_mstatus, 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))), $past(csrfile_mtvec, 2), $past(pcReg, 2), 1) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_LOAD_REQ_a: assert property (disable iff(rst) FETCH_DONE_to_LOAD_REQ_p);
property FETCH_DONE_to_LOAD_REQ_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_I_L) &&
  !((((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_LH) || (getInstrType(fromMemoryPort_sig_loadedData) == INSTR_LHU)) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_LW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  LOAD_REQ &&
  csrfile_mcause == $past(csrfile_mcause, 2) &&
  csrfile_mepc == $past(csrfile_mepc, 2) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == $past(csrfile_mstatus, 2) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == $past(excFlag, 2) &&
  pcReg == $past(pcReg, 2) &&
  phase == execute &&
  regfileWrite_dst == (($past(fromMemoryPort_sig_loadedData, 2) >> 7) & 31) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == getMemoryMask(getInstrType($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


FETCH_DONE_to_STORE_a: assert property (disable iff(rst) FETCH_DONE_to_STORE_p);
property FETCH_DONE_to_STORE_p;
    a_u_32_32 fromRegsPort_sig_at_t_1;
  ##1
  FETCH_DONE &&
  fromMemoryPort_sync &&
  !(phase == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData) == ENC_S) &&
  !(((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SH) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData) == INSTR_SW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData)), fromRegsPort_sig[((fromMemoryPort_sig_loadedData >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData)) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t_1, fromRegsPort_sig)
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*3] and
  ($past(fromMemoryPort_notify == 0, 0))[*3] and
  ($past(isa_ecall_Port_notify == 0, 0))[*3] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*3] and
  ##2
  STORE &&
  csrfile_mcause == $past(csrfile_mcause, 2) &&
  csrfile_mepc == $past(csrfile_mepc, 2) &&
  csrfile_mie == $past(csrfile_mie, 2) &&
  csrfile_mip == $past(mip_isa_Port_sig, 2) &&
  csrfile_misa == $past(csrfile_misa, 2) &&
  csrfile_mscratch == $past(csrfile_mscratch, 2) &&
  csrfile_mstatus == $past(csrfile_mstatus, 2) &&
  csrfile_mtval == $past(csrfile_mtval, 2) &&
  csrfile_mtvec == $past(csrfile_mtvec, 2) &&
  excFlag == $past(excFlag, 2) &&
  pcReg == $past(pcReg, 2) &&
  phase == execute &&
  regfileWrite_dst == $past(regfileWrite_dst, 2) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 2) &&
  toMemoryPort_sig_addrIn == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData, 2))), fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_sig_dataIn == fromRegsPort_sig_at_t_1[(($past(fromMemoryPort_sig_loadedData, 2) >> 20) & 31)] &&
  toMemoryPort_sig_mask == getMemoryMask(getInstrType($past(fromMemoryPort_sig_loadedData, 2))) &&
  toMemoryPort_sig_req == ME_WR &&
  toMemoryPort_notify == 1;
endproperty


FETCH_REQ_to_FETCH_DONE_a: assert property (disable iff(rst) FETCH_REQ_to_FETCH_DONE_p);
property FETCH_REQ_to_FETCH_DONE_p;
  ##1
  FETCH_REQ &&
  toMemoryPort_sync
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0)) and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##1
  FETCH_DONE &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  fromMemoryPort_notify == 1;
endproperty


LOAD_DONE_to_FETCH_REQ_a: assert property (disable iff(rst) LOAD_DONE_to_FETCH_REQ_p);
property LOAD_DONE_to_FETCH_REQ_p;
  ##1
  LOAD_DONE &&
  fromMemoryPort_sync
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0)) and
  ($past(toRegsPort_notify == 0, 0)) and
  ##1
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(fromMemoryPort_sig_loadedData, 1) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toRegsPort_sig_dst == $past(regfileWrite_dst, 1) &&
  toRegsPort_sig_dstData == $past(fromMemoryPort_sig_loadedData, 1) &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 1;
endproperty


LOAD_REQ_to_LOAD_DONE_a: assert property (disable iff(rst) LOAD_REQ_to_LOAD_DONE_p);
property LOAD_REQ_to_LOAD_DONE_p;
  ##1
  LOAD_REQ &&
  toMemoryPort_sync
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0)) and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0))[*2] and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##1
  LOAD_DONE &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  fromMemoryPort_notify == 1;
endproperty


STORE_to_FETCH_REQ_a: assert property (disable iff(rst) STORE_to_FETCH_REQ_p);
property STORE_to_FETCH_REQ_p;
  ##1
  STORE &&
  toMemoryPort_sync
|->
  ($past(ecall_isa_Port_notify == 0, 0))[*2] and
  ($past(fromMemoryPort_notify == 0, 0))[*2] and
  ($past(isa_ecall_Port_notify == 0, 0))[*2] and
  ($past(toMemoryPort_notify == 0, 0)) and
  ($past(toRegsPort_notify == 0, 0))[*2] and
  ##1
  FETCH_REQ &&
  csrfile_mcause == updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mepc == updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == updateMSTATUS(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == 0 &&
  pcReg == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  phase == fetch &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  toMemoryPort_sig_addrIn == updatePC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause, 1), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), $past(excFlag, 1)), $past(csrfile_mepc, 1), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)), $past(csrfile_mie, 1), $past(csrfile_mip, 1), $past(csrfile_misa, 1), $past(csrfile_mscratch, 1), $past(csrfile_mstatus, 1), $past(csrfile_mtval, 1), $past(csrfile_mtvec, 1), ($past(pcReg, 1) + 4), $past(excFlag, 1)) &&
  toMemoryPort_sig_dataIn == 0 &&
  toMemoryPort_sig_mask == MT_W &&
  toMemoryPort_sig_req == ME_RD &&
  toMemoryPort_notify == 1;
endproperty


ECALL_DONE_wait_a: assert property (disable iff(rst) ECALL_DONE_wait_p);
property ECALL_DONE_wait_p;
  ##1
  ECALL_DONE &&
  !ecall_isa_Port_sync
|->
  ##1
  ECALL_DONE &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  ecall_isa_Port_notify == 1 &&
  fromMemoryPort_notify == 0 &&
  isa_ecall_Port_notify == 0 &&
  toMemoryPort_notify == 0 &&
  toRegsPort_notify == 0;
endproperty


ECALL_REQ_wait_a: assert property (disable iff(rst) ECALL_REQ_wait_p);
property ECALL_REQ_wait_p;
  ##1
  ECALL_REQ &&
  !isa_ecall_Port_sync
|->
  ##1
  ECALL_REQ &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  isa_ecall_Port_sig == $past(isa_ecall_Port_sig, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  ecall_isa_Port_notify == 0 &&
  fromMemoryPort_notify == 0 &&
  isa_ecall_Port_notify == 1 &&
  toMemoryPort_notify == 0 &&
  toRegsPort_notify == 0;
endproperty


FETCH_DONE_wait_a: assert property (disable iff(rst) FETCH_DONE_wait_p);
property FETCH_DONE_wait_p;
  ##1
  FETCH_DONE &&
  !fromMemoryPort_sync
|->
  ##1
  FETCH_DONE &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  ecall_isa_Port_notify == 0 &&
  fromMemoryPort_notify == 1 &&
  isa_ecall_Port_notify == 0 &&
  toMemoryPort_notify == 0 &&
  toRegsPort_notify == 0;
endproperty


FETCH_REQ_wait_a: assert property (disable iff(rst) FETCH_REQ_wait_p);
property FETCH_REQ_wait_p;
  ##1
  FETCH_REQ &&
  !toMemoryPort_sync
|->
  ##1
  FETCH_REQ &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  toMemoryPort_sig_addrIn == $past(toMemoryPort_sig_addrIn, 1) &&
  toMemoryPort_sig_dataIn == $past(toMemoryPort_sig_dataIn, 1) &&
  toMemoryPort_sig_mask == $past(toMemoryPort_sig_mask, 1) &&
  toMemoryPort_sig_req == $past(toMemoryPort_sig_req, 1) &&
  ecall_isa_Port_notify == 0 &&
  fromMemoryPort_notify == 0 &&
  isa_ecall_Port_notify == 0 &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 0;
endproperty


LOAD_DONE_wait_a: assert property (disable iff(rst) LOAD_DONE_wait_p);
property LOAD_DONE_wait_p;
  ##1
  LOAD_DONE &&
  !fromMemoryPort_sync
|->
  ##1
  LOAD_DONE &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  ecall_isa_Port_notify == 0 &&
  fromMemoryPort_notify == 1 &&
  isa_ecall_Port_notify == 0 &&
  toMemoryPort_notify == 0 &&
  toRegsPort_notify == 0;
endproperty


LOAD_REQ_wait_a: assert property (disable iff(rst) LOAD_REQ_wait_p);
property LOAD_REQ_wait_p;
  ##1
  LOAD_REQ &&
  !toMemoryPort_sync
|->
  ##1
  LOAD_REQ &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  toMemoryPort_sig_addrIn == $past(toMemoryPort_sig_addrIn, 1) &&
  toMemoryPort_sig_dataIn == $past(toMemoryPort_sig_dataIn, 1) &&
  toMemoryPort_sig_mask == $past(toMemoryPort_sig_mask, 1) &&
  toMemoryPort_sig_req == $past(toMemoryPort_sig_req, 1) &&
  ecall_isa_Port_notify == 0 &&
  fromMemoryPort_notify == 0 &&
  isa_ecall_Port_notify == 0 &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 0;
endproperty


STORE_wait_a: assert property (disable iff(rst) STORE_wait_p);
property STORE_wait_p;
  ##1
  STORE &&
  !toMemoryPort_sync
|->
  ##1
  STORE &&
  csrfile_mcause == $past(csrfile_mcause, 1) &&
  csrfile_mepc == $past(csrfile_mepc, 1) &&
  csrfile_mie == $past(csrfile_mie, 1) &&
  csrfile_mip == $past(csrfile_mip, 1) &&
  csrfile_misa == $past(csrfile_misa, 1) &&
  csrfile_mscratch == $past(csrfile_mscratch, 1) &&
  csrfile_mstatus == $past(csrfile_mstatus, 1) &&
  csrfile_mtval == $past(csrfile_mtval, 1) &&
  csrfile_mtvec == $past(csrfile_mtvec, 1) &&
  excFlag == $past(excFlag, 1) &&
  pcReg == $past(pcReg, 1) &&
  phase == $past(phase, 1) &&
  regfileWrite_dst == $past(regfileWrite_dst, 1) &&
  regfileWrite_dstData == $past(regfileWrite_dstData, 1) &&
  toMemoryPort_sig_addrIn == $past(toMemoryPort_sig_addrIn, 1) &&
  toMemoryPort_sig_dataIn == $past(toMemoryPort_sig_dataIn, 1) &&
  toMemoryPort_sig_mask == $past(toMemoryPort_sig_mask, 1) &&
  toMemoryPort_sig_req == $past(toMemoryPort_sig_req, 1) &&
  ecall_isa_Port_notify == 0 &&
  fromMemoryPort_notify == 0 &&
  isa_ecall_Port_notify == 0 &&
  toMemoryPort_notify == 1 &&
  toRegsPort_notify == 0;
endproperty


endmodule


module ISS_Interrupts_bind(
  input bit clk
);


default clocking default_clk @(posedge clk); endclocking


ISS_Interrupts_verification inst(
  .rst(ISS_Interrupts.rst),
  .clk(clk),
  .ecall_isa_Port_sig(ISS_Interrupts.ecall_isa_Port),
  .fromMemoryPort_sig_loadedData(ISS_Interrupts.fromMemoryPort.loadedData),
  .fromRegsPort_sig(ISS_Interrupts.fromRegsPort),
  .fromRegsPort_sig_0(),
  .fromRegsPort_sig_1(),
  .fromRegsPort_sig_2(),
  .fromRegsPort_sig_3(),
  .fromRegsPort_sig_4(),
  .fromRegsPort_sig_5(),
  .fromRegsPort_sig_6(),
  .fromRegsPort_sig_7(),
  .fromRegsPort_sig_8(),
  .fromRegsPort_sig_9(),
  .fromRegsPort_sig_10(),
  .fromRegsPort_sig_11(),
  .fromRegsPort_sig_12(),
  .fromRegsPort_sig_13(),
  .fromRegsPort_sig_14(),
  .fromRegsPort_sig_15(),
  .fromRegsPort_sig_16(),
  .fromRegsPort_sig_17(),
  .fromRegsPort_sig_18(),
  .fromRegsPort_sig_19(),
  .fromRegsPort_sig_20(),
  .fromRegsPort_sig_21(),
  .fromRegsPort_sig_22(),
  .fromRegsPort_sig_23(),
  .fromRegsPort_sig_24(),
  .fromRegsPort_sig_25(),
  .fromRegsPort_sig_26(),
  .fromRegsPort_sig_27(),
  .fromRegsPort_sig_28(),
  .fromRegsPort_sig_29(),
  .fromRegsPort_sig_30(),
  .fromRegsPort_sig_31(),
  .isa_ecall_Port_sig(ISS_Interrupts.isa_ecall_Port),
  .mip_isa_Port_sig(ISS_Interrupts.mip_isa_Port),
  .toMemoryPort_sig_addrIn(ISS_Interrupts.toMemoryPort.addrIn),
  .toMemoryPort_sig_dataIn(ISS_Interrupts.toMemoryPort.dataIn),
  .toMemoryPort_sig_mask(ISS_Interrupts.toMemoryPort.mask),
  .toMemoryPort_sig_req(ISS_Interrupts.toMemoryPort.req),
  .toRegsPort_sig_dst(ISS_Interrupts.toRegsPort.dst),
  .toRegsPort_sig_dstData(ISS_Interrupts.toRegsPort.dstData),
  .ecall_isa_Port_sync(ISS_Interrupts.ecall_isa_Port_sync),
  .fromMemoryPort_sync(ISS_Interrupts.fromMemoryPort_sync),
  .isa_ecall_Port_sync(ISS_Interrupts.isa_ecall_Port_sync),
  .toMemoryPort_sync(ISS_Interrupts.toMemoryPort_sync),
  .ecall_isa_Port_notify(ISS_Interrupts.ecall_isa_Port_notify),
  .fromMemoryPort_notify(ISS_Interrupts.fromMemoryPort_notify),
  .isa_ecall_Port_notify(ISS_Interrupts.isa_ecall_Port_notify),
  .toMemoryPort_notify(ISS_Interrupts-toMemoryPort_notify),
  .toRegsPort_notify(ISS_Interrupts.toRegsPort_notify),
  .csrfile_mcause(ISS_Interrupts.csrfile.mcause),
  .csrfile_mepc(ISS_Interrupts.csrfile.mepc),
  .csrfile_mie(ISS_Interrupts.csrfile.mie),
  .csrfile_mip(ISS_Interrupts.csrfile.mip),
  .csrfile_misa(ISS_Interrupts.csrfile.misa),
  .csrfile_mscratch(ISS_Interrupts.csrfile.mscratch),
  .csrfile_mstatus(ISS_Interrupts.csrfile.mstatus),
  .csrfile_mtval(ISS_Interrupts.csrfile.mtval),
  .csrfile_mtvec(ISS_Interrupts.csrfile.mtvec),
  .excFlag(ISS_Interrupts.excFlag),
  .pcReg(ISS_Interrupts.pcReg_signal),
  .phase(ISS_Interrupts.phase),
  .regfileWrite_dst(ISS_Interrupts.toRegsPort.dst),
  .regfileWrite_dstData(ISS_Interrupts.toRegsPort.dstData),
  .FETCH_REQ(ISS_Interrupts.phase == fetch && ISS_Interrupts.toMemoryPort_notify  ==1'b1),
  .FETCH_DONE(ISS_Interrupts.phase == fetch && ISS_Interrupts.fromMemoryPort_notify==1'b1),
  .STORE(ISS_Interrupts.phase == execute && ISS_Interrupts.toMemoryPort_notify  == 1'b1 && ISS_Interrupts.opcode == ENC_S),
  .LOAD_REQ(ISS_Interrupts.phase == execute && ISS_Interrupts.toMemoryPort_notify  == 1'b1 && ISS_Interrupts.opcode == ENC_I_L),
  .LOAD_DONE(ISS_Interrupts.phase == execute && ISS_Interrupts.fromMemoryPort_notify  == 1'b1 && ISS_Interrupts.opcode == ENC_L),
  .ECALL_REQ(ISS_Interrupts.phase == execute && ISS_Interrupts.isa_ecall_Port_notify  ==1'b1),
  .ECALL_DONE(ISS_Interrupts.phase == execute && ISS_Interrupts.ecall_isa_Port_notify  ==1'b1)
);


endmodule


bind ISS_Interrupts ISS_Interrupts_bind inst(
  .clk(ISS_Interrupts.clk)
);
