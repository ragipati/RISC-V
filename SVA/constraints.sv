// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// RTL implementation of a RISCV ISA
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, Sandeep Ragipati
// -------------------------------------------------
// @lang=sva @ts=8

`include "ISS_Interrupts_datapath.sv"
`include "ISS_Interrupts_registers.sv"
`include "ISS_Interrupts_synchronization.sv"

module ISS_Interrupts_verification_constraints();

default clocking default_clk @(posedge ISS_Interrupts.clk); endclocking

//REGFILE Constarints
property REGISTER_FILE_CONSTRAINT;
    !toRegsPort_notify()
|=>
    `fromRegsPort_sig == $past(`fromRegsPort_sig);
endproperty
REGISTER_FILE_CONSTRAINT_a: assume property(REGISTER_FILE_CONSTRAINT);

property REGISTER_FILE_ZERO_CONSTRAINT;
    `fromRegsPort_sig[0] == 32'b0;
endproperty
REGISTER_FILE_ZERO_CONSTRAINT_a: assume property(REGISTER_FILE_ZERO_CONSTRAINT);

endmodule
bind ISS_Interrupts ISS_Interrupts_verification_constraints constraints(.*);
